<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720130544 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE named_armor (id INT AUTO_INCREMENT NOT NULL, brand_id_id INT DEFAULT NULL, gear_type_id_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, main_attribute VARCHAR(255) NOT NULL, unique_attribute VARCHAR(255) DEFAULT NULL, second_attribute1 VARCHAR(255) NOT NULL, second_attribute2 VARCHAR(255) DEFAULT NULL, perfect_talent LONGTEXT DEFAULT NULL, mods VARCHAR(255) NOT NULL, drop_location LONGTEXT NOT NULL, INDEX IDX_9903708524BD5740 (brand_id_id), INDEX IDX_990370856402ECDE (gear_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE named_armor ADD CONSTRAINT FK_9903708524BD5740 FOREIGN KEY (brand_id_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE named_armor ADD CONSTRAINT FK_990370856402ECDE FOREIGN KEY (gear_type_id_id) REFERENCES gear_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE named_armor');
    }
}
