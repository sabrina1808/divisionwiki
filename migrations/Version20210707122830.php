<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210707122830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE weapon_talent (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, name_eng VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, requirements VARCHAR(255) DEFAULT NULL, rifle VARCHAR(255) NOT NULL, shotgun VARCHAR(255) NOT NULL, assault_rifle VARCHAR(255) NOT NULL, light_machine_gun VARCHAR(255) NOT NULL, marksman_rifle VARCHAR(255) NOT NULL, pistol VARCHAR(255) NOT NULL, sub_machine_gun VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE weapon_talent');
    }
}
