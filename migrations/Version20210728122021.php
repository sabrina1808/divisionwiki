<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210728122021 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE max_roll_armor (id INT AUTO_INCREMENT NOT NULL, gear_type_id_id INT DEFAULT NULL, weapon_damage INT NOT NULL, armor INT NOT NULL, skill_tier INT NOT NULL, critical_hit_chance INT NOT NULL, critical_hit_damage INT NOT NULL, headshot_damage INT NOT NULL, weapon_handling INT NOT NULL, armor_regeneration INT NOT NULL, eplosive_resistance INT NOT NULL, hazard_resistance INT NOT NULL, health INT NOT NULL, skill_damage INT NOT NULL, skill_hate INT NOT NULL, skill_repair INT NOT NULL, status_effects INT NOT NULL, UNIQUE INDEX UNIQ_CE1060F06402ECDE (gear_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE max_roll_armor ADD CONSTRAINT FK_CE1060F06402ECDE FOREIGN KEY (gear_type_id_id) REFERENCES gear_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE max_roll_armor');
    }
}
