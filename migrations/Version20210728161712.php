<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210728161712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE weapon_model (id INT AUTO_INCREMENT NOT NULL, weapon_type_id_id INT DEFAULT NULL, variant_weapon VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, optimal_range INT NOT NULL, rpm INT NOT NULL, mag_size INT NOT NULL, reload_speed DOUBLE PRECISION NOT NULL, headshot_multiplicator INT NOT NULL, weapon_bonus VARCHAR(255) NOT NULL, bonus_max_roll INT NOT NULL, base_damage INT NOT NULL, INDEX IDX_FA2F82BC4E1FD613 (weapon_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE weapon_model ADD CONSTRAINT FK_FA2F82BC4E1FD613 FOREIGN KEY (weapon_type_id_id) REFERENCES weapon_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE weapon_model');
    }
}
