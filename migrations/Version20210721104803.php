<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721104803 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exotic_armor (id INT AUTO_INCREMENT NOT NULL, gear_type_id_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, talent1 LONGTEXT NOT NULL, talent2 LONGTEXT DEFAULT NULL, main_attribute VARCHAR(255) NOT NULL, second_attribute1 VARCHAR(255) DEFAULT NULL, second_attribute2 VARCHAR(255) DEFAULT NULL, mods VARCHAR(255) NOT NULL, warlords VARCHAR(255) NOT NULL, INDEX IDX_F341FDCE6402ECDE (gear_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exotic_armor ADD CONSTRAINT FK_F341FDCE6402ECDE FOREIGN KEY (gear_type_id_id) REFERENCES gear_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE exotic_armor');
    }
}
