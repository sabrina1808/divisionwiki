<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210722110507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gear_set_model (id INT AUTO_INCREMENT NOT NULL, gear_type_id_id INT DEFAULT NULL, gear_set_id_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, main_attribute VARCHAR(255) NOT NULL, second_attribute VARCHAR(255) NOT NULL, mods VARCHAR(255) NOT NULL, INDEX IDX_D3BCB9E26402ECDE (gear_type_id_id), INDEX IDX_D3BCB9E27B6D556C (gear_set_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gear_set_model ADD CONSTRAINT FK_D3BCB9E26402ECDE FOREIGN KEY (gear_type_id_id) REFERENCES gear_type (id)');
        $this->addSql('ALTER TABLE gear_set_model ADD CONSTRAINT FK_D3BCB9E27B6D556C FOREIGN KEY (gear_set_id_id) REFERENCES gear_set (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE gear_set_model');
    }
}
