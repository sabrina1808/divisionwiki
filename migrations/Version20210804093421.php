<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210804093421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE max_roll_weapon (id INT AUTO_INCREMENT NOT NULL, weapon_type_id_id INT DEFAULT NULL, weapon_damage INT NOT NULL, health_damage DOUBLE PRECISION NOT NULL, damage_to_armor INT NOT NULL, critical_hit_chance DOUBLE PRECISION NOT NULL, damage_to_target_out_of_cover INT NOT NULL, headshot_damage INT NOT NULL, critical_hit_damage INT NOT NULL, reload_speed INT NOT NULL, stability INT NOT NULL, accurancy INT NOT NULL, optimal_range INT NOT NULL, magazine_size DOUBLE PRECISION NOT NULL, rate_of_fire INT NOT NULL, swap_speed INT NOT NULL, UNIQUE INDEX UNIQ_60F4DC534E1FD613 (weapon_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE max_roll_weapon ADD CONSTRAINT FK_60F4DC534E1FD613 FOREIGN KEY (weapon_type_id_id) REFERENCES weapon_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE max_roll_weapon');
    }
}
