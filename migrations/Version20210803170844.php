<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210803170844 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exotic_weapon (id INT AUTO_INCREMENT NOT NULL, weapon_type_id_id INT DEFAULT NULL, variant_weapon VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, talent1 LONGTEXT NOT NULL, talent2 LONGTEXT DEFAULT NULL, main_attribute1 VARCHAR(255) NOT NULL, main_attribute2 VARCHAR(255) NOT NULL, second_attribute VARCHAR(255) NOT NULL, optimal_range INT NOT NULL, rpm INT NOT NULL, mag_size INT NOT NULL, relaod_speed DOUBLE PRECISION NOT NULL, headshot_multiplicator INT NOT NULL, base_damage INT NOT NULL, mods LONGTEXT NOT NULL, warlords VARCHAR(255) NOT NULL, INDEX IDX_A1A890654E1FD613 (weapon_type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exotic_weapon ADD CONSTRAINT FK_A1A890654E1FD613 FOREIGN KEY (weapon_type_id_id) REFERENCES weapon_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE exotic_weapon');
    }
}
