<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FloatFilter extends AbstractExtension {
  public function getFilters()
  {
    return [
      new TwigFilter('floatFilter', [$this, 'formatNumber'])
    ];
  }

  public function formatNumber($numberReturn) {
    $number = number_format($numberReturn, 1, ',', '');

    return $number;
  }
}