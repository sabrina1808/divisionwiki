<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class IntegerFilter extends AbstractExtension {
  public function getFilters()
  {
    return [
      new TwigFilter('integerFilter', [$this, 'formatNumber'])
    ];
  }

  public function formatNumber($numberReturn, $separator = " ") {
    $number = number_format($numberReturn, 0, '.', $separator);

    return $number;
  }
}