<?php

namespace App\Controller\Brands;

use App\Repository\BrandRepository;
use App\Repository\GearSetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BrandsListController extends AbstractController {
    private $brandRepo;
    private $gearSetRepo;

    /**
     * @param BrandRepository $brandRepository
     * @param GearSetRepository $gearSetRepository
     */
    public function __construct(BrandRepository $brandRepository, GearSetRepository $gearSetRepository)
    {
        $this->brandRepo = $brandRepository;
        $this->gearSetRepo = $gearSetRepository;
    }

    /**
     * @return Response
     */
    public function index() : Response {
        $brands = $this->brandRepo->getAllBrandByAlphabeticOrder();
        $gearSets = $this->gearSetRepo->getAllGearSetByAlphabeticOrder();

        return $this->render('brands/index.html.twig', [
            'brands' => $brands,
            'gearSets' => $gearSets
        ]);
    }
}