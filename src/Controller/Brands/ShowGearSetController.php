<?php

namespace App\Controller\Brands;

use App\Repository\GearSetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowGearSetController extends AbstractController {
    private $gearSetRepo;

    /**
     * @param GearSetRepository $gearSetRepository
     */
    public function __construct(GearSetRepository $gearSetRepository)
    {
        $this->gearSetRepo = $gearSetRepository;
    }

    /**
     * @param integer $id   Identifiant du gearSet
     * @return Response
     */
    public function showGearSet(int $id) : Response {
        $gearSet = $this->gearSetRepo->find($id);

        $gearSetModel = $gearSet->getGearSetModels();

        return $this->render('brands/showGearSet.html.twig', [
            'gearSet' => $gearSet,
            'gearSetModels' => $gearSetModel
        ]);
    }
}