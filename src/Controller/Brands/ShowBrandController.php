<?php

namespace App\Controller\Brands;

use App\Repository\BrandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowBrandController extends AbstractController {
    private $brandRepo;

    /**
     * @param BrandRepository $brandRepository
     */
    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepo = $brandRepository;
    }

    /**
     * @param integer $id   Identifiant de la marque
     * @return Response
     */
    public function showBrand(int $id) : Response {
        $brand = $this->brandRepo->find($id);

        $armors = $brand->getArmorModels();
        $namedArmors = $brand->getNamedArmors();

        return $this->render('brands/showBrand.html.twig', [
            'brand' => $brand,
            'armors' => $armors,
            'namedArmors' => $namedArmors
        ]);
    }
}