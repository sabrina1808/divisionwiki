<?php

namespace App\Controller\Admin\MaxRollWeapon;

use App\Form\MaxRollWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddMaxRollWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addMaxRollWeapon(Request $request) : Response {
        $form = $this->createForm(MaxRollWeaponType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $maxRoll = $form->getData();

            $this->em->persist($maxRoll);
            $this->em->flush();

            return $this->redirectToRoute('adminMaxRollWeaponList');
        }

        return $this->render('admin/maxRollWeapon/add.html.twig', [
            'rollForm' => $form->createView()
        ]);
    }
}