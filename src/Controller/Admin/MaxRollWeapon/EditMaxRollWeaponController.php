<?php

namespace App\Controller\Admin\MaxRollWeapon;

use App\Entity\MaxRollWeapon;
use App\Form\MaxRollWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditMaxRollWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param MaxRollWeapon $maxRollWeapon
     * @return Response
     */
    public function editMaxRollWeapon(Request $request, MaxRollWeapon $maxRollWeapon) : Response {
        $form = $this->createForm(MaxRollWeaponType::class, $maxRollWeapon);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminMaxRollWeaponList');
        }

        return $this->render('admin/maxRollWeapon/edit.html.twig', [
            'rollForm' => $form->createView()
        ]);
    }
}