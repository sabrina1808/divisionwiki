<?php

namespace App\Controller\Admin\MaxRollWeapon;

use App\Repository\MaxRollWeaponRepository;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminMaxRollWeaponListController extends AbstractController {
    private $maxRollWeaponRepo;

    /**
     * @param MaxRollWeaponRepository $maxRollWeaponRepository
     */
    public function __construct(MaxRollWeaponRepository $maxRollWeaponRepository)
    {
        $this->maxRollWeaponRepo = $maxRollWeaponRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @return Response
     */
    public function index() : Response {
        $maxRolls = $this->maxRollWeaponRepo->findAll();

        return $this->render('admin/maxRollWeapon/index.html.twig', [
            'maxRolls' => $maxRolls
        ]);
    }
}