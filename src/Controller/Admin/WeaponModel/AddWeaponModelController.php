<?php

namespace App\Controller\Admin\WeaponModel;

use App\Form\WeaponModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddWeaponModelController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addWeaponModel(Request $request) : Response {
        $form = $this->createForm(WeaponModelType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $weaponModel = $form->getData();

            $this->em->persist($weaponModel);
            $this->em->flush();

            return $this->redirectToRoute('adminWeaponModelList');
        }

        return $this->render('admin/weaponModel/add.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}