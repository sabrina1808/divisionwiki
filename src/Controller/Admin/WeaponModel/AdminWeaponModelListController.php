<?php

namespace App\Controller\Admin\WeaponModel;

use App\Entity\WeaponModel;
use App\Repository\WeaponModelRepository;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminWeaponModelListController extends AbstractController {
    private $weaponModelRepo;

    /**
     * @param WeaponModelRepository $weaponModelRepository
     */
    public function __construct(WeaponModelRepository $weaponModelRepository)
    {
        $this->weaponModelRepo = $weaponModelRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @return Response
     */
    public function index() : Response {
        /** @var WeaponModel */
        $weapons = $this->weaponModelRepo->findAll();

        $shotgun = [];
        $rifle = [];
        $assautlRifle = [];
        $lightMachineGun = [];
        $marksmanRifle = [];
        $pistol = [];
        $subMachineGun = [];
        foreach($weapons as $weapon) {
            $weaponType = $weapon->getWeaponTypeId()->getId();
            switch($weaponType) {
                case 1:
                    array_push($shotgun, $weapon);
                    break;
                case 2:
                    array_push($rifle, $weapon);
                    break;
                case 3:
                    array_push($assautlRifle, $weapon);
                    break;
                case 4:
                    array_push($lightMachineGun, $weapon);
                    break;
                case 5:
                    array_push($marksmanRifle, $weapon);
                    break;
                case 6:
                    array_push($pistol, $weapon);
                    break;
                case 7:
                    array_push($subMachineGun, $weapon);
                    break;
            }
        }

        return $this->render('admin/weaponModel/index.html.twig', [
            'shotguns' => $shotgun,
            'rifles' => $rifle,
            'assaultRifles' => $assautlRifle,
            'lightMachineGuns' => $lightMachineGun,
            'marksmanRifles' => $marksmanRifle,
            'pistols' => $pistol,
            'subMachineGuns' => $subMachineGun 
        ]);
    }
}