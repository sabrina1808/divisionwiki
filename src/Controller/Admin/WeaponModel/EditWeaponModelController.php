<?php

namespace App\Controller\Admin\WeaponModel;

use App\Entity\WeaponModel;
use App\Form\WeaponModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EditWeaponModelController extends AbstractController {
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function editWeaponModel(Request $request, WeaponModel $weaponModel) : Response {
        $form = $this->createForm(WeaponModelType::class, $weaponModel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminWeaponModelList');
        }

        return $this->render('admin/weaponModel/edit.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}