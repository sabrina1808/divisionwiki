<?php

namespace App\Controller\Admin\GearSet;

use App\Entity\GearSet;
use App\Form\GearSetType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class EditGearSetController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @param GearSet $gearSet
     * @return Response
     */
    public function editGearSet(Request $request, GearSet $gearSet) : Response {
        $form = $this->createForm(GearSetType::class, $gearSet);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminGearSetList');
        }

        return $this->render('admin/gearSet/edit.html.twig', [
            'gearForm' => $form->createView()
        ]);
    }
}