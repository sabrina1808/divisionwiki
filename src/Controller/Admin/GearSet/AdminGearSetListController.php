<?php

namespace App\Controller\Admin\GearSet;

use App\Repository\GearSetRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminGearSetListController extends AbstractController {
    private $gearSetRepo;

    /**
     * @param GearSetRepository $gearSetRepository
     */
    public function __construct(GearSetRepository $gearSetRepository)
    {
        $this->gearSetRepo = $gearSetRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response
     */
    public function index() : Response {
        $gearSet = $this->gearSetRepo->findAll();

        return $this->render('admin/gearSet/index.html.twig', [
            'gearSets' => $gearSet
        ]);
    }
}