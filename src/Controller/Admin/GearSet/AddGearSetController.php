<?php

namespace App\Controller\Admin\GearSet;

use App\Form\GearSetType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AddGearSetController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @return Response
     */
    public function addGearSet(Request $request) : Response {
        $form = $this->createForm(GearSetType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $gearSet = $form->getData();

            $this->em->persist($gearSet);
            $this->em->flush();

            return $this->redirectToRoute('adminGearSetList');
        }

        return $this->render('admin/gearSet/add.html.twig', [
            'gearForm' => $form->createView()
        ]);
    }
}