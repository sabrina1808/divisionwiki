<?php

namespace App\Controller\Admin\MaxRollArmor;

use App\Form\MaxRollArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddMaxRollArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addMaxRollArmor(Request $request) : Response {
        $form = $this->createForm(MaxRollArmorType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $roll = $form->getData();

            $this->em->persist($roll);
            $this->em->flush();

            return $this->redirectToRoute('adminMaxRollArmorList');
        }

        return $this->render('admin/maxRollArmor/add.html.twig', [
            'rollForm' => $form->createView()
        ]);
    }
}