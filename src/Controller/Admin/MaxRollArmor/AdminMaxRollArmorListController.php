<?php

namespace App\Controller\Admin\MaxRollArmor;

use App\Repository\MaxRollArmorRepository;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminMaxRollArmorListController extends AbstractController {
    private $maxRollRepo;

    /**
     * @param MaxRollArmorRepository $maxRollArmorRepository
     */
    public function __construct(MaxRollArmorRepository $maxRollArmorRepository)
    {
        $this->maxRollRepo = $maxRollArmorRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @return Response
     */
    public function index() : Response {
        $maxRoll = $this->maxRollRepo->findAll();

        return $this->render('admin/maxRollArmor/index.html.twig', [
            'maxRolls' => $maxRoll
        ]);
    }
}