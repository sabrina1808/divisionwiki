<?php

namespace App\Controller\Admin\MaxRollArmor;

use App\Entity\MaxRollArmor;
use App\Form\MaxRollArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditMaxRollArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param MaxRollArmor $maxRollArmor
     * @return Response
     */
    public function editMaxRollArmor(Request $request, MaxRollArmor $maxRollArmor) : Response {
        $form = $this->createForm(MaxRollArmorType::class, $maxRollArmor);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminMaxRollArmorList');
        }

        return $this->render('admin/maxRollArmor/edit.html.twig', [
            'rollForm' => $form->createView()
        ]);
    }
}