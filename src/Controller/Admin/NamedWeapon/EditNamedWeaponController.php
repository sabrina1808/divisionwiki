<?php

namespace App\Controller\Admin\NamedWeapon;

use App\Entity\NamedWeapon;
use App\Form\NamedWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditNamedWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param NamedWeapon $namedWeapon
     * @return Response
     */
    public function editNamedWeapon(Request $request, NamedWeapon $namedWeapon) : Response {
        $form = $this->createForm(NamedWeaponType::class, $namedWeapon);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminNamedWeaponList');
        }

        return $this->render('admin/namedWeapon/edit.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}