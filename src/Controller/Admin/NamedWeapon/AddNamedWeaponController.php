<?php

namespace App\Controller\Admin\NamedWeapon;

use App\Form\NamedWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddNamedWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addNamedWeapon(Request $request) : Response {
        $form = $this->createForm(NamedWeaponType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $namedWeapon = $form->getData();

            $this->em->persist($namedWeapon);
            $this->em->flush();

            return $this->redirectToRoute('adminNamedWeaponList');
        }

        return $this->render('admin/namedWeapon/add.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}