<?php

namespace App\Controller\Admin\ExoticArmor;

use App\Entity\ExoticArmor;
use App\Repository\ExoticArmorRepository;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminExoticArmorListController extends AbstractController {
    private $exoticArmorRepo;

    /**
     * @param ExoticArmorRepository $exoticArmorRepository
     */
    public function __construct(ExoticArmorRepository $exoticArmorRepository)
    {
        $this->exoticArmorRepo = $exoticArmorRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response
     */
    public function index() : Response {
        /** @var ExoticArmor */
        $exoticArmor = $this->exoticArmorRepo->findAll();

        $mask = [];
        $chest = [];
        $backpack = [];
        $gloves = [];
        $holster = [];
        $kneepads = [];
        foreach($exoticArmor as $armor) {
            $gearId = $armor->getGearTypeId()->getId();
            switch ($gearId) {
                case 1:
                    array_push($mask, $armor);
                    break;
                case 2:
                    array_push($chest, $armor);
                    break;
                case 3:
                    array_push($backpack, $armor);
                    break;
                case 4:
                    array_push($gloves, $armor);
                    break;
                case 5:
                    array_push($holster, $armor);
                    break;
                case 6:
                    array_push($kneepads, $armor);
                    break;
            }
        }

        return $this->render('admin/exoticArmor/index.html.twig', [
            'masks' => $mask,
            'chests' => $chest,
            'backpacks' => $backpack,
            'gloves' => $gloves,
            'holsters' => $holster,
            'kneepads' => $kneepads
        ]);
    }
}