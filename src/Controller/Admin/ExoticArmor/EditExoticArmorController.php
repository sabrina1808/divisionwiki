<?php

namespace App\Controller\Admin\ExoticArmor;

use App\Entity\ExoticArmor;
use App\Form\ExoticArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditExoticArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param ExoticArmor $exoticArmor
     * @return Response
     */
    public function editExoticArmor(Request $request, ExoticArmor $exoticArmor) : Response {
        $form = $this->createForm(ExoticArmorType::class, $exoticArmor);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            
            return $this->redirectToRoute('adminExoticArmorList');
        }

        return $this->render('admin/exoticArmor/edit.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}