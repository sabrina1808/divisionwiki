<?php

namespace App\Controller\Admin\ExoticArmor;

use App\Form\ExoticArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddExoticArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addExoticArmor(Request $request) : Response {
        $form = $this->createForm(ExoticArmorType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $exoticArmor = $form->getData();

            $this->em->persist($exoticArmor);
            $this->em->flush();

            return $this->redirectToRoute('adminExoticArmorList');
        }

        return $this->render('admin/exoticArmor/add.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}