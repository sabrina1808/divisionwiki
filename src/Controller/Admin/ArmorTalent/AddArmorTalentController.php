<?php

namespace App\Controller\Admin\ArmorTalent;

use App\Form\ArmorTalentType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddArmorTalentController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @return Response
     */
    public function addArmorTalent(Request $request) : Response {
        $form = $this->createForm(ArmorTalentType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $armorTalent = $form->getData();

            $this->em->persist($armorTalent);
            $this->em->flush();

            return $this->redirectToRoute('adminArmorTalentList');
        }

        return $this->render('admin/armorTalent/add.html.twig', [
            'talentForm' => $form->createView()
        ]);
    }
}