<?php

namespace App\Controller\Admin\ArmorTalent;

use App\Repository\ArmorTalentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminArmorTalentListController extends AbstractController {
    private $armorTalentRepo;

    /**
     * @param ArmorTalentRepository $armorTalentRepository
     */
    public function __construct(ArmorTalentRepository $armorTalentRepository)
    {
        $this->armorTalentRepo = $armorTalentRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response
     */
    public function index() : Response {
        $armorTalentList = $this->armorTalentRepo->findAll();

        return $this->render('admin/armorTalent/index.html.twig', [
            'talents' => $armorTalentList
        ]);
    }
}