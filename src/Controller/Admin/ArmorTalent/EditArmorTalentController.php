<?php

namespace App\Controller\Admin\ArmorTalent;

use App\Entity\ArmorTalent;
use App\Form\ArmorTalentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditArmorTalentController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @param ArmorTalent $armorTalent
     * @return Response
     */
    public function editArmorTalent(Request $request, ArmorTalent $armorTalent) : Response {
        $form =  $this->createForm(ArmorTalentType::class, $armorTalent);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminArmorTalentList');
        }

        return $this->render('admin/armorTalent/edit.html.twig', [
            'talentForm' => $form->createView()
        ]);
    }
}