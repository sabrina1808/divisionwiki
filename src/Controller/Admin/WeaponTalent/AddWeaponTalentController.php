<?php

namespace App\Controller\Admin\WeaponTalent;

use App\Form\WeaponTalentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AddWeaponTalentController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @return Response
     */
    public function addWeaponTalent(Request $request) : Response {
        $form = $this->createForm(WeaponTalentType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $weaponTalent = $form->getData();

            $this->em->persist($weaponTalent);
            $this->em->flush();

            return $this->redirectToRoute('adminWeaponTalentList');
        }

        return $this->render('admin/weaponTalent/add.html.twig', [
            'talentForm' => $form->createView()
        ]);
    }
}