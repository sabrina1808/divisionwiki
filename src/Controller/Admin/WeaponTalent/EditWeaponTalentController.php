<?php

namespace App\Controller\Admin\WeaponTalent;

use App\Entity\WeaponTalent;
use App\Form\WeaponTalentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditWeaponTalentController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param WeaponTalent $weaponTalent
     * @return Response
     */
    public function editWeaponTalent(Request $request, WeaponTalent $weaponTalent) : Response {
        $form = $this->createForm(WeaponTalentType::class, $weaponTalent);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminWeaponTalentList');
        }

        return $this->render('admin/weaponTalent/edit.html.twig', [
            'talentForm' => $form->createView()
        ]);
    }
}