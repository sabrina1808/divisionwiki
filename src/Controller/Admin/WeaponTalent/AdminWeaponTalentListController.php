<?php

namespace App\Controller\Admin\WeaponTalent;

use App\Repository\WeaponTalentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminWeaponTalentListController extends AbstractController {
    private $weaponTalentRepo;

    /**
     * @param WeaponTalentRepository $weaponTalentRepository
     */
    public function __construct(WeaponTalentRepository $weaponTalentRepository)
    {
        $this->weaponTalentRepo = $weaponTalentRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response
     */
    public function index() : Response {
        $weaponTalentList = $this->weaponTalentRepo->findAll();

        return $this->render('admin/weaponTalent/index.html.twig', [
            'talents' => $weaponTalentList
        ]);
    }
}