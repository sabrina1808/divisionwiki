<?php

namespace App\Controller\Admin\NamedArmor;

use App\Entity\NamedArmor;
use App\Form\NamedArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditNamedArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param NamedArmor $namedArmor
     * @return Response
     */
    public function editNamedArmor(Request $request, NamedArmor $namedArmor) : Response {
        $form = $this->createForm(NamedArmorType::class, $namedArmor);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminNamedArmorList');
        }

        return $this->render('admin/namedArmor/edit.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}