<?php

namespace App\Controller\Admin\NamedArmor;

use App\Form\NamedArmorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddNamedArmorController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addNamedArmor(Request $request) : Response {
        $form = $this->createForm(NamedArmorType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $namedArmor = $form->getData();

            $this->em->persist($namedArmor);
            $this->em->flush();

            return $this->redirectToRoute('adminNamedArmorList');
        }

        return $this->render('admin/namedArmor/add.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}