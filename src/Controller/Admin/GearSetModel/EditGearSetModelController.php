<?php

namespace App\Controller\Admin\GearSetModel;

use App\Entity\GearSetModel;
use App\Form\GearSetModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditGearSetModelController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param GearSetModel $gearSetModel
     * @return Response
     */
    public function editGearSetModel(Request $request, GearSetModel $gearSetModel) : Response {
        $form = $this->createForm(GearSetModelType::class, $gearSetModel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminGearSetModelList');
        }

        return $this->render('admin/gearSetModel/edit.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}