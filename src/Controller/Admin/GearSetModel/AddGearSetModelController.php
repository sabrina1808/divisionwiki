<?php

namespace App\Controller\Admin\GearSetModel;

use App\Form\GearSetModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddGearSetModelController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addGearSetModel(Request $request) : Response {
        $form = $this->createForm(GearSetModelType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $gearSetModel = $form->getData();

            $this->em->persist($gearSetModel);
            $this->em->flush();

            return $this->redirectToRoute('adminGearSetModelList');
        }

        return $this->render('admin/gearSetModel/add.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}