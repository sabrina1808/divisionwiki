<?php

namespace App\Controller\Admin\ArmorModel;

use App\Entity\ArmorModel;
use App\Form\ArmorModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditArmorModelController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param ArmorModel $armorModel
     * @return Response
     */
    public function editArmorModel(Request $request, ArmorModel $armorModel) : Response {
        $form = $this->createForm(ArmorModelType::class, $armorModel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminArmorModelList');
        }

        return $this->render('admin/armorModel/edit.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}