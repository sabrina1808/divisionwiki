<?php

namespace App\Controller\Admin\ArmorModel;

use App\Form\ArmorModelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddArmorModelController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @return Response
     */
    public function addArmorModel(Request $request) : Response {
        $form = $this->createForm(ArmorModelType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $armorModel = $form->getData();

            $this->em->persist($armorModel);
            $this->em->flush();

            return $this->redirectToRoute('adminArmorModelList');
        }

        return $this->render('admin/armorModel/add.html.twig', [
            'armorForm' => $form->createView()
        ]);
    }
}