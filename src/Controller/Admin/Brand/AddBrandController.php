<?php

namespace App\Controller\Admin\Brand;

use App\Form\BrandType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AddBrandController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @return Response
     */
    public function addBrand(Request $request) : Response {
        $form = $this->createForm(BrandType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $brand = $form->getData();
            
            $this->em->persist($brand);
            $this->em->flush();

            return $this->redirectToRoute('adminBrandList');
        }

        return $this->render('admin/brand/add.html.twig', [
            'brandForm' => $form->createView()
        ]);
    }
}