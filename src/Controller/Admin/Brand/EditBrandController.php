<?php

namespace App\Controller\Admin\Brand;

use App\Entity\Brand;
use App\Form\BrandType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class EditBrandController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @param Request $request
     * @param Brand $brand
     * @return Response
     */
    public function editBrand(Request $request, Brand $brand) : Response {
        $form = $this->createForm(BrandType::class, $brand);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminBrandList');
        }

        return $this->render('admin/brand/edit.html.twig', [
            'brandForm' => $form->createView()
        ]);
    }
}