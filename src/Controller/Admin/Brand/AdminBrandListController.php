<?php

namespace App\Controller\Admin\Brand;

use App\Repository\BrandRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminBrandListController extends AbstractController {
    private $brandRepo;

    /**
     * @param BrandRepository $brandRepository
     */
    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepo = $brandRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response
     */
    public function index() : Response {
        $brands = $this->brandRepo->findAll();

        return $this->render('admin/brand/index.html.twig', [
            'brands' => $brands
        ]);
    }
}