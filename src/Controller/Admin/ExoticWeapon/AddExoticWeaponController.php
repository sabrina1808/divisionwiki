<?php

namespace App\Controller\Admin\ExoticWeapon;

use App\Form\ExoticWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddExoticWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addExoticWeapon(Request $request) : Response {
        $form = $this->createForm(ExoticWeaponType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $exoticWeapon = $form->getData();

            $this->em->persist($exoticWeapon);
            $this->em->flush();

            return $this->redirectToRoute('adminExoticWeaponList');
        }

        return $this->render('admin/exoticWeapon/add.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}