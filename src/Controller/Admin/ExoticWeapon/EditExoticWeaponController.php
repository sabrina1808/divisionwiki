<?php

namespace App\Controller\Admin\ExoticWeapon;

use App\Entity\ExoticWeapon;
use App\Form\ExoticWeaponType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EditExoticWeaponController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param ExoticWeapon $exoticWeapon
     * @return Response
     */
    public function editExoticWeapon(Request $request, ExoticWeapon $exoticWeapon) : Response {
        $form = $this->createForm(ExoticWeaponType::class, $exoticWeapon);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('adminExoticWeaponList');
        }

        return $this->render('admin/exoticWeapon/edit.html.twig', [
            'weaponForm' => $form->createView()
        ]);
    }
}