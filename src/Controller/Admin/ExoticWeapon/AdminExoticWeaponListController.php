<?php

namespace App\Controller\Admin\ExoticWeapon;

use App\Entity\ExoticWeapon;
use App\Repository\ExoticWeaponRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminExoticWeaponListController extends AbstractController {
    private $exoticWeaponRepo;

    /**
     * @param ExoticWeaponRepository $exoticWeaponRepository
     */
    public function __construct(ExoticWeaponRepository $exoticWeaponRepository)
    {
        $this->exoticWeaponRepo = $exoticWeaponRepository;
    }

    public function index() : Response {
        /** @var ExoticWeapon */
        $exoticWeapons = $this->exoticWeaponRepo->findAll();

        $shotgun = [];
        $rifle = [];
        $assautlRifle = [];
        $lightMachineGun = [];
        $marksmanRifle = [];
        $pistol = [];
        $subMachineGun = [];
        foreach($exoticWeapons as $weapon) {
            $weaponType = $weapon->getWeaponTypeId()->getId();
            switch($weaponType) {
                case 1:
                    array_push($shotgun, $weapon);
                    break;
                case 2:
                    array_push($rifle, $weapon);
                    break;
                case 3:
                    array_push($assautlRifle, $weapon);
                    break;
                case 4:
                    array_push($lightMachineGun, $weapon);
                    break;
                case 5:
                    array_push($marksmanRifle, $weapon);
                    break;
                case 6:
                    array_push($pistol, $weapon);
                    break;
                case 7:
                    array_push($subMachineGun, $weapon);
                    break;
            }
        }

        return $this->render('admin/exoticWeapon/index.html.twig', [
            'shotguns' => $shotgun,
            'rifles' => $rifle,
            'assaultRifles' => $assautlRifle,
            'lightMachineGuns' => $lightMachineGun,
            'marksmanRifles' => $marksmanRifle,
            'pistols' => $pistol,
            'subMachineGuns' => $subMachineGun
        ]);
    }
}