<?php

namespace App\Controller\PlayerStats;

use App\Form\PlayerStatsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlayerStatsController extends AbstractController {
    private $client;

    /**
     * @param HttpClientInterface $httpClientInterface
     */
    public function __construct(HttpClientInterface $httpClientInterface)
    {
        $this->client = $httpClientInterface;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getStats(Request $request) : Response {
        $form = $this->createForm(PlayerStatsType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $response = $this->client->request(
				'GET',
				'https://public-api.tracker.gg/v2/division-2/standard/profile/' . $data['plateform'] . '/' . $data['pseudo'],
				array(
					'headers' => [
						'TRN-Api-Key' => '35dbd8dd-7fda-483d-bdc2-750e75d98def'
					]
				)
			);

			$content = $response->toArray();

            return $this->render('playerStats/stats.html.twig', [
                'content' => $content
            ]);
        }

        return $this->render('playerStats/index.html.twig', [
            'statsForm' => $form->createView()
        ]);
    }
}