<?php

namespace App\Controller\Armors;

use App\Repository\NamedArmorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowNamedArmorController extends AbstractController {
    private $namedArmorRepo;

    /**
     * @param NamedArmorRepository $namedArmorRepository
     */
    public function __construct(NamedArmorRepository $namedArmorRepository)
    {
        $this->namedArmorRepo = $namedArmorRepository;
    }

    /**
     * @param integer $id   Identifiant de l'armure nommée
     * @return Response
     */
    public function showNamedArmor(int $id) : Response {
        $namedArmor = $this->namedArmorRepo->find($id);

        return $this->render('armors/showNamedArmor.html.twig', [
            'namedArmor' => $namedArmor
        ]);
    }
}