<?php

namespace App\Controller\Armors;

use App\Repository\GearSetModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowGearSetModelController extends AbstractController {
    private $gearSetModelRepo;

    /**
     * @param GearSetModelRepository $gearSetModelRepository
     */
    public function __construct(GearSetModelRepository $gearSetModelRepository)
    {
        $this->gearSetModelRepo = $gearSetModelRepository;
    }

    /**
     * @param integer $id   Identifiant du modèle de gear set
     * @return Response
     */
    public function showGearSetModel(int $id) : Response {
        $gearSetModel = $this->gearSetModelRepo->find($id);

        $otherModels = $this->gearSetModelRepo->getAllGearSetModelByGearSetId($gearSetModel->getGearSetId()->getId());

        return $this->render('armors/showGearSetModel.html.twig', [
            'gearSetModel' => $gearSetModel,
            'otherGearSets' => $otherModels
        ]);
    }
}