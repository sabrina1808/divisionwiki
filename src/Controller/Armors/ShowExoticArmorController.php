<?php

namespace App\Controller\Armors;

use App\Repository\ExoticArmorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowExoticArmorController extends AbstractController {
    private $exoticArmorRepo;

    /**
     * @param ExoticArmorRepository $exoticArmorRepository
     */
    public function __construct(ExoticArmorRepository $exoticArmorRepository)
    {
        $this->exoticArmorRepo = $exoticArmorRepository;
    }

    /**
     * @param integer $id   Identifiant de l'armure exotique
     * @return Response
     */
    public function showExoticArmor(int $id) : Response {
        $exoticArmor = $this->exoticArmorRepo->find($id);

        return $this->render('armors/showExoticArmor.html.twig', [
            'exoticArmor' => $exoticArmor
        ]);
    }
}