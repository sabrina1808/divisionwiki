<?php

namespace App\Controller\Armors;

use App\Repository\ArmorModelRepository;
use App\Repository\ExoticArmorRepository;
use App\Repository\GearSetModelRepository;
use App\Repository\MaxRollArmorRepository;
use App\Repository\NamedArmorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ArmorsListController extends AbstractController {
    private $armorModelRepo;
    private $namedArmorRepo;
    private $exoticArmorRepo;
    private $gearSetModelRepo;
    private $maxRollArmorRepo;

    /**
     * @param ArmorModelRepository $armorModelRepository
     * @param NamedArmorRepository $namedArmorRepository
     * @param ExoticArmorRepository $exoticArmorRepository
     * @param GearSetModelRepository $gearSetModelRepository
     * @param MaxRollArmorRepository $maxRollArmorRepository
     */
    public function __construct(ArmorModelRepository $armorModelRepository, NamedArmorRepository $namedArmorRepository, ExoticArmorRepository $exoticArmorRepository, GearSetModelRepository $gearSetModelRepository, MaxRollArmorRepository $maxRollArmorRepository)
    {
        $this->armorModelRepo = $armorModelRepository;
        $this->namedArmorRepo = $namedArmorRepository;
        $this->exoticArmorRepo = $exoticArmorRepository;
        $this->gearSetModelRepo = $gearSetModelRepository;
        $this->maxRollArmorRepo = $maxRollArmorRepository;
    }

    /**
     * @return Response
     */
    public function index() : Response {
        $armorModels = $this->armorModelRepo->findAll();
        $namedArmors = $this->namedArmorRepo->findAll();
        $exoticArmors = $this->exoticArmorRepo->findAll();
        $gearSetModels = $this->gearSetModelRepo->findAll();
        $maxRollArmors = $this->maxRollArmorRepo->findAll();

        return $this->render('armors/index.html.twig',[
            'armorModels' => $armorModels,
            'namedArmors' => $namedArmors,
            'exoticArmors' => $exoticArmors,
            'gearSetModels' => $gearSetModels,
            'maxRollArmor' => $maxRollArmors
        ]);
    }
}