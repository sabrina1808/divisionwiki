<?php

namespace App\Controller\Armors;

use App\Repository\ArmorModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowArmorController extends AbstractController {
    private $armorRepo;

    /**
     * @param ArmorModelRepository $armorModelRepository
     */
    public function __construct(ArmorModelRepository $armorModelRepository)
    {
        $this->armorRepo = $armorModelRepository;
    }

    /**
     * @param integer $id   Identifiant du modèle d'armure
     * @return Response
     */
    public function showArmor(int $id) : Response {
        $armor = $this->armorRepo->find($id);

        $otherArmors = $this->armorRepo->getAllArmorByBrandId($armor->getBrandId()->getId());

        return $this->render('armors/showArmor.html.twig', [
            'armor' => $armor,
            'otherArmors' => $otherArmors
        ]);
    }
}