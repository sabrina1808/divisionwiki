<?php

namespace App\Controller\Talents;

use App\Repository\ArmorTalentRepository;
use App\Repository\NamedArmorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowArmorTalentController extends AbstractController {
    private $armorTalentRepo;
    private $namedArmorRepo;

    /**
     * @param ArmorTalentRepository $armorTalentRepository
     * @param NamedArmorRepository $namedArmorRepository
     */
    public function __construct(ArmorTalentRepository $armorTalentRepository, NamedArmorRepository $namedArmorRepository)
    {
        $this->armorTalentRepo = $armorTalentRepository;
        $this->namedArmorRepo = $namedArmorRepository;
    }

    /**
     * @param integer $id   Identifiant du talent d'armure
     * @return Response
     */
    public function showArmorTalent(int $id) : Response {
        $armorTalent = $this->armorTalentRepo->find($id);

        $talentName = $armorTalent->getName();

        $perfectTalent = $this->namedArmorRepo->findPerfectTalent($talentName);

        $chestTalent = $this->armorTalentRepo->getAllChestTalent();
        $backpackTalent = $this->armorTalentRepo->getAllBackpackTalent();

        return $this->render('talents/showArmorTalent.html.twig', [
            'armorTalent' => $armorTalent,
            'chestTalents' => $chestTalent,
            'backpackTalents' => $backpackTalent,
            'perfectTalent' => $perfectTalent,
        ]);
    }
}