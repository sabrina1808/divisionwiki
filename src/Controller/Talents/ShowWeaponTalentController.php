<?php

namespace App\Controller\Talents;

use App\Repository\NamedWeaponRepository;
use App\Repository\WeaponTalentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowWeaponTalentController extends AbstractController {
    private $weaponTalentRepo;
    private $namedWeaponRepo;

    /**
     * @param WeaponTalentRepository $weaponTalentRepository
     * @param NamedWeaponRepository $namedWeaponRepository
     */
    public function __construct(WeaponTalentRepository $weaponTalentRepository, NamedWeaponRepository $namedWeaponRepository)
    {
        $this->weaponTalentRepo = $weaponTalentRepository;
        $this->namedWeaponRepo = $namedWeaponRepository;
    }

    /**
     * @param integer $id   Identifiant du talent d'arme
     * @return Response
     */
    public function showWeaponTalent(int $id) : Response {
        $weaponTalent = $this->weaponTalentRepo->find($id);

        $talentName = $weaponTalent->getName();
        
        $perfectTalent = $this->namedWeaponRepo->findPerfectTalent($talentName);

        $allWeaponTalents = $this->weaponTalentRepo->getAllWeaponTalentByAlphabeticOrder();

        return $this->render('talents/showWeaponTalent.html.twig', [
            'weaponTalent' => $weaponTalent,
            'allTalents' => $allWeaponTalents,
            'perfectTalent' => $perfectTalent
        ]);
    }
}