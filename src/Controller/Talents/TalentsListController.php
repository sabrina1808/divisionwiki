<?php

namespace App\Controller\Talents;

use App\Repository\ArmorTalentRepository;
use App\Repository\WeaponTalentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TalentsListController extends AbstractController {
    private $armorTalentRepo;
    private $weaponTalentRepo;

    /**
     * @param ArmorTalentRepository $armorTalentRepository
     * @param WeaponTalentRepository $weaponTalentRepository
     */
    public function __construct(ArmorTalentRepository $armorTalentRepository, WeaponTalentRepository $weaponTalentRepository)
    {
        $this->armorTalentRepo = $armorTalentRepository;
        $this->weaponTalentRepo = $weaponTalentRepository;
    }

    /**
     * @return Response
     */
    public function index() : Response {
        $armorTalents = $this->armorTalentRepo->getAllArmorTalentByAlphabeticOrder();
        $weaponTalents = $this->weaponTalentRepo->getAllWeaponTalentByAlphabeticOrder();

        return $this->render('talents/index.html.twig', [
            'armorTalents' => $armorTalents,
            'weaponTalents' => $weaponTalents
        ]);
    }
}