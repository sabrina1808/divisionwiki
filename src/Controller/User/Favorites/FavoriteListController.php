<?php

namespace App\Controller\User\Favorites;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FavoriteListController extends AbstractController {
    /**
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function showFavorite() : Response {
        return $this->render('user/favorites/showFavorite.html.twig');
    }
}