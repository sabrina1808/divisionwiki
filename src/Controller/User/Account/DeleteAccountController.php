<?php

namespace App\Controller\User\Account;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteAccountController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_USER")
     * 
     * @param integer $id   Identifiant de l'utilisateur
     * @param Request $request
     * @return Response
     */
    public function deleteAccount(int $id, Request $request) : Response {
        /** @var User */
        $user = $this->getUser();

        if($user->getId() !== $id) {
            throw new NotFoundHttpException();
        }

        $this->container->get('security.token_storage')->setToken(null);

        if($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $this->em->remove($user);
            $this->em->flush();
        }

        return $this->redirectToRoute('home');
    }
}