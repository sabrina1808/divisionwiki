<?php

namespace App\Controller\User\Account;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountInformationController extends AbstractController {
    /**
     * @IsGranted("ROLE_USER")
     * 
     * @param integer $id   Identifiant de l'utilisateur
     * @return Response
     */
    public function showInformations(int $id) : Response {
        /** @var User */
        $user = $this->getUser();

        if($user->getId() !== $id) {
            throw new NotFoundHttpException();
        }

        return $this->render('user/account/showInformations.html.twig', [
            'user' => $user
        ]);
    }
}