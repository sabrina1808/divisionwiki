<?php

namespace App\Controller\User\Account;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EditUserInformationController extends AbstractController {
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @IsGranted("ROLE_USER")
     * 
     * @param integer $id   Identifiant de l'utilisateur
     * @param Request $request
     * @return Response
     */
    public function editUserInformations(int $id, Request $request) : Response {
        /** @var User */  
        $user = $this->getUser();

        if($user->getId() !== $id) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('showUserInformations', ['id' => $user->getId()]);
        }
        
        return $this->render('user/account/editInformations.html.twig', [
            'userForm' => $form->createView()
        ]);
    }
}