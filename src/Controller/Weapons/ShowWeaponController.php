<?php

namespace App\Controller\Weapons;

use App\Repository\WeaponModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowWeaponController extends AbstractController {
    private $weaponModelRepo;

    /**
     * @param WeaponModelRepository $weaponModelRepository
     */
    public function __construct(WeaponModelRepository $weaponModelRepository)
    {
        $this->weaponModelRepo = $weaponModelRepository;
    }

    /**
     * @param integer $id   Identifiant de l'arme
     * @return Response
     */
    public function showWeapon(int $id) : Response {
        $weapon = $this->weaponModelRepo->find($id);

        $variantWeapons = $this->weaponModelRepo->getAllWeaponByVariantName($weapon->getVariantWeapon(), $weapon->getId());

        return $this->render('weapons/showWeapon.html.twig', [
            'weapon' => $weapon,
            'variantWeapons' => $variantWeapons
        ]);
    }
}