<?php

namespace App\Controller\Weapons;

use App\Repository\NamedWeaponRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowNamedWeaponController extends AbstractController {
    private $namedWeaponRepo;

    /**
     * @param NamedWeaponRepository $namedWeaponRepository
     */
    public function __construct(NamedWeaponRepository $namedWeaponRepository)
    {
        $this->namedWeaponRepo = $namedWeaponRepository;
    }

    /**
     * @param integer $id   Identifiant de l'arme nommée
     * @return Response
     */
    public function showNamedWeapon(int $id) : Response {
        $namedWeapon = $this->namedWeaponRepo->find($id);
        dump($namedWeapon);

        return $this->render('weapons/showNamedWeapon.html.twig', [
            'namedWeapon' => $namedWeapon
        ]);
    }
}