<?php

namespace App\Controller\Weapons;

use App\Repository\ExoticWeaponRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShowExoticWeaponController extends AbstractController {
    private $exoticWeaponRepo;

    /**
     * @param ExoticWeaponRepository $exoticWeaponRepository
     */
    public function __construct(ExoticWeaponRepository $exoticWeaponRepository)
    {
        $this->exoticWeaponRepo = $exoticWeaponRepository;
    }

    /**
     * @param integer $id   Identifiant de l'arme exotique
     * @return Response
     */
    public function showExoticWeapon(int $id) : Response {
        $exoticWeapon = $this->exoticWeaponRepo->find($id);

        return $this->render('weapons/showExoticWeapon.html.twig', [
            'exoticWeapon' => $exoticWeapon
        ]);
    }
}