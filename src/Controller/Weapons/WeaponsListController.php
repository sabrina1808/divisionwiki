<?php

namespace App\Controller\Weapons;

use App\Repository\ExoticWeaponRepository;
use App\Repository\MaxRollWeaponRepository;
use App\Repository\NamedWeaponRepository;
use App\Repository\WeaponModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class WeaponsListController extends AbstractController {
    private $weaponModelRepo;
    private $namedWeaponRepo;
    private $exoticWeaponRepo;
    private $maxRollWeaponRepo;

    /**
     * @param WeaponModelRepository $weaponModelRepository
     * @param NamedWeaponRepository $namedWeaponRepository
     * @param ExoticWeaponRepository $exoticWeaponRepository
     * @param MaxRollWeaponRepository $maxRollWeaponRepository
     */
    public function __construct(WeaponModelRepository $weaponModelRepository, NamedWeaponRepository $namedWeaponRepository, ExoticWeaponRepository $exoticWeaponRepository, MaxRollWeaponRepository $maxRollWeaponRepository)
    {
        $this->weaponModelRepo = $weaponModelRepository;
        $this->namedWeaponRepo = $namedWeaponRepository;
        $this->exoticWeaponRepo = $exoticWeaponRepository;
        $this->maxRollWeaponRepo = $maxRollWeaponRepository;
    }

    /**
     * @return Response
     */
    public function index() : Response {
        $weapons = $this->weaponModelRepo->findAll();
        $namedWeapons = $this->namedWeaponRepo->findAll();
        $exoticWeapons = $this->exoticWeaponRepo->findAll();
        $maxRollWeapons = $this->maxRollWeaponRepo->findAll();

        return $this->render('weapons/index.html.twig', [
            'weapons' => $weapons,
            'namedWeapons' => $namedWeapons,
            'exoticWeapons' => $exoticWeapons,
            'maxRollWeapons' => $maxRollWeapons
        ]);
    }
}