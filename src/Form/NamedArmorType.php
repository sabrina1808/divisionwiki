<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\GearType;
use App\Entity\NamedArmor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NamedArmorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gearTypeId', EntityType::class, [
                'label' => 'Type d\'armure',
                'class' => GearType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('brandId', EntityType::class, [
                'label' => 'Marque',
                'class' => Brand::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'armure'
                ]
            ])
            ->add('mainAttribute', ChoiceType::class, [
                'label' => 'Attribut principal',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts d\'armes' => NamedArmor::OFFENSIF,
                    'Protection' => NamedArmor::DEFENSIF,
                    'Tier de compétence' => NamedArmor::TIER
                ]
            ])
            ->add('uniqueAttribute', TextType::class, [
                'label' => 'Attribut unique',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Attribut unique'
                ]
            ])
            ->add('secondAttribute1', TextType::class, [
                'label' => 'Attribut secondaire 1',
                'data' => 'Aléatoire parmi offensif, défensif et utilitaire',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('secondAttribute2', TextType::class, [
                'label' => 'Attribut secondaire 2',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Attribut secondaire 2'
                ]
            ])
            ->add('perfectTalent', TextareaType::class, [
                'label' => 'Talent parfait',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent parfait'
                ]
            ])
            ->add('mods', ChoiceType::class, [
                'label' => 'Mods',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => NamedArmor::NO,
                    'Oui' => NamedArmor::YES
                ]
            ])
            ->add('dropLocation', TextareaType::class, [
                'label' => 'Zone d\'obtention de l\'armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Zone d\'obtention de l\'armure'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NamedArmor::class,
        ]);
    }
}
