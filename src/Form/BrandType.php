<?php

namespace App\Form;

use App\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la marque',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de la marque',
                    'autofocus' => true
                ]
            ])
            ->add('bonus1', TextType::class, [
                'label' => 'Bonus 1 pièce',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 1 pièce'
                ]
            ])
            ->add('bonus2', TextType::class, [
                'label' => 'Bonus 2 pièces',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 2 pièces'
                ]
            ])
            ->add('bonus3', TextType::class, [
                'label' => 'Bonus 3 pièces',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 3 pièces'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Description'
                ]
            ])
            ->add('warlord', ChoiceType::class, [
                'label' => 'Apparu avec Warlords ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non apparue avec Warlord Of New York' => Brand::NO,
                    'Apparue avec Warlords Of New York' => Brand::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Brand::class
        ]);
    }
}
