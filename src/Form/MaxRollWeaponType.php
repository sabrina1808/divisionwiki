<?php

namespace App\Form;

use App\Entity\WeaponType;
use App\Entity\MaxRollWeapon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class MaxRollWeaponType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weaponTypeId', EntityType::class, [
                'label' => 'Type d\'arme',
                'class' => WeaponType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('weaponDamage', IntegerType::class, [
                'label' => 'Dégâts d\'armes',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts d\'armes'
                ]
            ])
            ->add('healthDamage', NumberType::class, [
                'label' => 'Dégâts à la santé',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts à la santé'
                ]
            ])
            ->add('damageToArmor', IntegerType::class, [
                'label' => 'Dégâts aux protections',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts aux protections'
                ]
            ])
            ->add('criticalHitChance', NumberType::class, [
                'label' => 'Probabilité de coup critique',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Probabilité de coup critique'
                ]
            ])
            ->add('damageToTargetOutOfCover', IntegerType::class, [
                'label' => 'Dégâts sur cible non abritée',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts sur cible non abritée'
                ]
            ])
            ->add('headshotDamage', IntegerType::class, [
                'label' => 'Dégâts de headshot',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts de headshot'
                ]
            ])
            ->add('criticalHitDamage', IntegerType::class, [
                'label' => 'Dégâts de coup critique',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts de coup critique'
                ]
            ])
            ->add('reloadSpeed', IntegerType::class, [
                'label' => 'Vitesse de rechargement',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Vitesse de rechargement'
                ]
            ])
            ->add('stability', IntegerType::class, [
                'label' => 'Stabilité',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Stabilité'
                ]
            ])
            ->add('accurancy', IntegerType::class, [
                'label' => 'Précision',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Précision'
                ]
            ])
            ->add('optimalRange', IntegerType::class, [
                'label' => 'Portée optimale',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Portée optimale'
                ]
            ])
            ->add('magazineSize', NumberType::class, [
                'label' => 'Taille du chargeur',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Taille du chargeur'
                ]
            ])
            ->add('rateOfFire', IntegerType::class, [
                'label' => 'Cadence de tir',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Cadence de tir'
                ]
            ])
            ->add('swapSpeed', IntegerType::class, [
                'label' => 'Vitesse de changement d\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Vitesse de changement d\'arme'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MaxRollWeapon::class,
        ]);
    }
}
