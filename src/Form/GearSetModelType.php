<?php

namespace App\Form;

use App\Entity\GearSet;
use App\Entity\GearType;
use App\Entity\GearSetModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GearSetModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gearTypeId', EntityType::class, [
                'label' => 'Type d\'armure',
                'class' => GearType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('gearSetId', EntityType::class, [
                'label' => 'Gear Set',
                'class' => GearSet::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'armure'
                ]
            ])
            ->add('mainAttribute', ChoiceType::class, [
                'label' => 'Attribut principal',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts d\'armes' => GearSetModel::OFFENSIF,
                    'Protection' => GearSetModel::DEFENSIF,
                    'Tier de compétence' => GearSetModel::TIER
                ]
            ])
            ->add('secondAttribute', TextType::class, [
                'label' => 'Attribut secondaire',
                'data' => 'Aléatoire parmi offensif, défensif et utilitaire',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mods', ChoiceType::class, [
                'label' => 'Mods',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => GearSetModel::NO,
                    'Oui' => GearSetModel::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GearSetModel::class,
        ]);
    }
}
