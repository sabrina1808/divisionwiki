<?php

namespace App\Form;

use App\Entity\GearType;
use App\Entity\MaxRollArmor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class MaxRollArmorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gearTypeId', EntityType::class, [
                'label' => 'Type d\'armure',
                'class' => GearType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('weaponDamage', IntegerType::class, [
                'label' => 'Dégâts d\'armes',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts d\'armes'
                ]
            ])
            ->add('armor', IntegerType::class, [
                'label' => 'Armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Armure'
                ]
            ])
            ->add('skillTier', IntegerType::class, [
                'label' => 'Tier de compétence',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Tier de compétence'
                ]
            ])
            ->add('criticalHitChance', IntegerType::class, [
                'label' => 'Probabilité de coup critique',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Probabilité de coup critique'
                ]
            ])
            ->add('criticalHitDamage', IntegerType::class, [
                'label' => 'Dégâts de coup critique',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts de coup critique'
                ]
            ])
            ->add('headshotDamage', IntegerType::class, [
                'label' => 'Dégâts de headshot',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts de headshot'
                ]
            ])
            ->add('weaponHandling', IntegerType::class, [
                'label' => 'Maniement d\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Maniement d\'arme'
                ]
            ])
            ->add('armorRegeneration', IntegerType::class, [
                'label' => 'Régénération de protection',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Régénération de protection'
                ]
            ])
            ->add('eplosiveResistance', IntegerType::class, [
                'label' => 'Résistance Explosions',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Résistance Explosions'
                ]
            ])
            ->add('hazardResistance', IntegerType::class, [
                'label' => 'Protection contre les altérations d`états',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Protection contre les altérations d`états'
                ]
            ])
            ->add('health', IntegerType::class, [
                'label' => 'Santé',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Santé'
                ]
            ])
            ->add('skillDamage', IntegerType::class, [
                'label' => 'Dégâts de compétences',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Dégâts de compétences'
                ]
            ])
            ->add('skillHate', IntegerType::class, [
                'label' => 'Récupération de compétence',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Récupération de compétence'
                ]
            ])
            ->add('skillRepair', IntegerType::class, [
                'label' => 'Compétence de réparation',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Compétence de réparation'
                ]
            ])
            ->add('statusEffects', IntegerType::class, [
                'label' => 'Effet d\'état',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Effet d\'état'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MaxRollArmor::class,
        ]);
    }
}
