<?php

namespace App\Form;

use App\Entity\WeaponModel;
use App\Entity\WeaponType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeaponModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weaponTypeId', EntityType::class, [
                'label' => 'Type d\'arme',
                'class' => WeaponType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('variantWeapon', TextType::class, [
                'label' => 'Variante de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Variante de l\'arme'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'arme'
                ]
            ])
            ->add('optimalRange', IntegerType::class, [
                'label' => 'Portée optimale',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Portée optimale'
                ]
            ])
            ->add('rpm', IntegerType::class, [
                'label' => 'Cadence de tir',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Cadence de tir'
                ]
            ])
            ->add('magSize', IntegerType::class, [
                'label' => 'Taille du chargeur',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Taille du chargeur'
                ]
            ])
            ->add('reloadSpeed', NumberType::class, [
                'label' => 'Vitesse de rechargement',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Vitesse de rechargement'
                ]
            ])
            ->add('headshotMultiplicator', IntegerType::class, [
                'label' => 'Multiplacateur de headshot',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Multiplacateur de headshot'
                ]
            ])
            ->add('weaponBonus', ChoiceType::class, [
                'label' => 'Bonus de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                ],
                'choices' => [
                    'Aucun' => 'Aucun',
                    'Dégâts aux protections' => WeaponModel::DAMAGE_TO_ARMOR,
                    'Dégâts de coup critique' => WeaponModel::CRITICAL_HIT_DAMAGE,
                    'Dégâts à la santé' => WeaponModel::HEALTH_DAMAGE,
                    'Dégâts sur cible non abritée' => WeaponModel::DAMAGE_TO_TARGET_OUT_OF_COVER,
                    'Dégâts de headshot' => WeaponModel::HEADSHOT_DAMAGE,
                    'Probablilité de coup critique' => WeaponModel::CRITICAL_HIT_CHANCE
                ]
            ])
            ->add('bonusMaxRoll', IntegerType::class, [
                'label' => 'Bonus max',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Bonus max'
                ]
            ])
            ->add('baseDamage', IntegerType::class, [
                'label' => 'Dégâts de base',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Dégâts de base'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WeaponModel::class,
        ]);
    }
}
