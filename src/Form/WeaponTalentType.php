<?php

namespace App\Form;

use App\Entity\WeaponTalent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class WeaponTalentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du talent',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom du talent',
                    'autofocus' => true
                ]
            ])
            ->add('nameEng', TextType::class, [
                'label' => 'Nom du talent (anglais)',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom du talent (anglais)'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Description'
                ]
            ])
            ->add('requirements', TextType::class, [
                'label' => 'Pré-requis',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Pré-requis'
                ]
            ])
            ->add('rifle', ChoiceType::class, [
                'label' => 'Disponible sur Fusil ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('shotgun', ChoiceType::class, [
                'label' => 'Disponible sur Fusil de calibre 12 ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('assaultRifle', ChoiceType::class, [
                'label' => 'Disponible sur Fusil d\'assaut ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('lightMachineGun', ChoiceType::class, [
                'label' => 'Disponible sur Fusil-mitrailleur ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('marksmanRifle', ChoiceType::class, [
                'label' => 'Disponible sur Fusil de précision ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('pistol', ChoiceType::class, [
                'label' => 'Disponible sur Pistolet ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
            ->add('subMachineGun', ChoiceType::class, [
                'label' => 'Disponible sur Pistolet-mitrailleur ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Oui' => WeaponTalent::YES,
                    'Non' => WeaponTalent::NO
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WeaponTalent::class
        ]);
    }
}
