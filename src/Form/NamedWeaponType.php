<?php

namespace App\Form;

use PhpParser\Node\Name;
use App\Entity\WeaponType;
use App\Entity\NamedWeapon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NamedWeaponType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weaponTypeId', EntityType::class, [
                'label' => 'Type d\'arme',
                'class' => WeaponType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('variantWeapon', TextType::class, [
                'label' => 'Variante de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Variante de l\'arme'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'arme'
                ]
            ])
            ->add('mainAttribute1', TextType::class, [
                'label' => 'Attribut essentiel 1',
                'data' => 'Dégâts d\'armes',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('mainAttribute2', ChoiceType::class, [
                'label' => 'Attribut essentiel 2',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Aucun' => NULL,
                    'Dégâts aux protections' => NamedWeapon::DAMAGE_TO_ARMOR,
                    'Dégâts de coup critique' => NamedWeapon::CRITICAL_HIT_DAMAGE,
                    'Dégâts à la santé' => NamedWeapon::HEALTH_DAMAGE,
                    'Dégâts sur cible non abritée' => NamedWeapon::DAMAGE_TO_TARGET_OUT_OF_COVER,
                    'Dégâts de headshot' => NamedWeapon::HEADSHOT_DAMAGE,
                    'Probabilité de coup critique' => NamedWeapon::CRITICAL_HIT_CHANCE
                ]
            ])
            ->add('secondAttribute', ChoiceType::class, [
                'label' => 'Attribut secondaire',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Alétoire' => NamedWeapon::RANDOM,
                    'Tier de compétence' => NamedWeapon::TIER,
                    'Dégâts aux protections' => NamedWeapon::DAMAGE_TO_ARMOR,
                    'Dégâts de headshot' => NamedWeapon::HEADSHOT_DAMAGE,
                    'Dégâts sur cible non abritée' => NamedWeapon::DAMAGE_TO_TARGET_OUT_OF_COVER,
                    'Vitesse de rechargement' => NamedWeapon::RELOAD_SPEED,
                    'Protection pour élimination' => NamedWeapon::ARMOR_ON_KILL,
                    'Stabilité' => NamedWeapon::STABILITY
                ]
            ])
            ->add('optimalRange', IntegerType::class, [
                'label' => 'Portée optimale',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Portée optimale'
                ]
            ])
            ->add('rpm', IntegerType::class, [
                'label' => 'Cadence de tir',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Cadence de tir'
                ]
            ])
            ->add('magSize', IntegerType::class, [
                'label' => 'Taille du chargeur',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Taille du chargeur'
                ]
            ])
            ->add('reloadSpeed', NumberType::class, [
                'label' => 'Vitesse de rechargement',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Vitesse de rechargement'
                ]
            ])
            ->add('headshotMultiplicator', IntegerType::class, [
                'label' => 'Multiplacateur de headshot',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Multiplacateur de headshot'
                ]
            ])
            ->add('baseDamage', IntegerType::class, [
                'label' => 'Dégâts de base',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Dégâts de base'
                ]
            ])
            ->add('perfectTalent', TextareaType::class, [
                'label' => 'Talent parfait',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent parfait'
                ]
            ])
            ->add('dropLocation', TextareaType::class, [
                'label' => 'Zone d\'obtention',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Zone d\'obtention'
                ]
            ])
            ->add('comments', TextareaType::class, [
                'label' => 'Commentaires',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Commentaires'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NamedWeapon::class,
        ]);
    }
}
