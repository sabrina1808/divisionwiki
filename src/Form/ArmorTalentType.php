<?php

namespace App\Form;

use App\Entity\ArmorTalent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArmorTalentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du talent',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom du talent',
                    'autofocus' => true
                ]
            ])
            ->add('nameEng', TextType::class, [
                'label' => 'Nom du talent (anglais)',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom du talent (anglais)'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Description'
                ]
            ])
            ->add('requirements', TextType::class, [
                'label' => 'Pré-requis',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Pré-requis'
                ]
            ])
            ->add('chest', ChoiceType::class, [
                'label' => 'Talent de torse ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => ArmorTalent::NO,
                    'Oui' => ArmorTalent::YES
                ]
            ])
            ->add('backpack', ChoiceType::class, [
                'label' => 'Talent de sac à dos',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => ArmorTalent::NO,
                    'Oui' => ArmorTalent::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArmorTalent::class
        ]);
    }
}
