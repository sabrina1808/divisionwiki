<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PlayerStatsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo', TextType::class, [
                'label' => 'Pseudo du joueur',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Pseudo du joueur',
                    'autofocus' => true
                ]
            ])
            ->add('plateform', ChoiceType::class, [
                'label' => 'Plateforme de jeu',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Xbox' => 'xbl',
					'Play' => 'psn',
					'PC' => 'uplay'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
