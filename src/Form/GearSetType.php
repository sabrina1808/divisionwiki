<?php

namespace App\Form;

use App\Entity\GearSet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class GearSetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du Gear Set',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom du Gear Set',
                    'autofocus' => true,
                ]
            ])
            ->add('bonus1', TextType::class, [
                'label' => 'Bonus 2 pièces',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 2 pièces'
                ]
            ])
            ->add('bonus2', TextType::class, [
                'label' => 'Bonus 3 pièces',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 3 pièces'
                ]
            ])
            ->add('bonus3', TextareaType::class, [
                'label' => 'Bonus 4 pièces',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Bonus 4 pièces'
                ]
            ])
            ->add('chestTalent', TextareaType::class, [
                'label' => 'Talent de torse',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent de tose'
                ]
            ])
            ->add('backpackTalent', TextareaType::class, [
                'label' => 'Talent de sac à dos',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent de sac à dos'
                ]
            ])
            ->add('warlord', ChoiceType::class, [
                'label' => 'Apparu avec Warlords ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non apparue avec Warlord Of New York' => GearSet::NO,
                    'Apparue avec Warlords Of New York' => GearSet::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GearSet::class
        ]);
    }
}
