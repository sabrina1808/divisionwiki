<?php

namespace App\Form;

use App\Entity\ArmorModel;
use App\Entity\Brand;
use App\Entity\GearType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArmorModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gearTypeId', EntityType::class, [
                'label' => 'Type d\'armure',
                'class' => GearType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('brandId', EntityType::class, [
                'label' => 'Marque',
                'class' => Brand::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'armure'
                ]
            ])
            ->add('mainAttribute', ChoiceType::class, [
                'label' => 'Attribut principal',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts d\'armes' => ArmorModel::OFFENSIF,
                    'Protection' => ArmorModel::DEFENSIF,
                    'Tier de compétence' => ArmorModel::TIER
                ]
            ])
            ->add('secondAttribute1', TextType::class, [
                'label' => 'Attribut secondaire 1',
                'data' => 'Aléatoire parmi offensif, défensif et utilitaire',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('secondAttribute2',  TextType::class, [
                'label' => 'Attribut secondaire 2',
                'data' => 'Aléatoire parmi offensif, défensif et utilitaire',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mods', ChoiceType::class, [
                'label' => 'Mods',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => ArmorModel::NO,
                    'Oui' => ArmorModel::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArmorModel::class,
        ]);
    }
}
