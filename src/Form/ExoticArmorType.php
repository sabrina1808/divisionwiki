<?php

namespace App\Form;

use App\Entity\GearType;
use App\Entity\ExoticArmor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExoticArmorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gearTypeId', EntityType::class, [
                'label' => 'Type d\'armure',
                'class' => GearType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'armure',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'armure'
                ]
            ])
            ->add('talent1', TextareaType::class, [
                'label' => 'Talent',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent'
                ]
            ])
            ->add('talent2', TextareaType::class, [
                'label' => 'Second talent',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Second talent'
                ]
            ])
            ->add('mainAttribute', ChoiceType::class, [
                'label' => 'Attribut principal',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts d\'armes' => ExoticArmor::OFFENSIF,
                    'Protection' => ExoticArmor::DEFENSIF,
                    'Tier de compétence' => ExoticArmor::TIER,
                    'Dégâts d\'armes/Protection/Tier de compétence' => ExoticArmor::ALLATTR
                ]
            ])
            ->add('secondAttribute1', ChoiceType::class, [
                'label' => 'Attribut secondaire',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Aucun' => NULL,
                    'Probabilité de coup critique' => ExoticArmor::CRITICAL_HIT_CHANCE,
                    'Maniement d\'arme' => ExoticArmor::WEAPON_HANDLING,
                    'Résitance explosion' => ExoticArmor::EXPLOSIVE_RESISTANCE,
                    'Dégâts de compétence' => ExoticArmor::SKILL_DAMAGE,
                    'Récupération de compétence' => ExoticArmor::SKILL_HASTE,
                    'Effet d\'état' => ExoticArmor::STATUS_EFFECT
                ]
            ])
            ->add('secondAttribute2', ChoiceType::class, [
                'label' => 'Attribut secondaire 2',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Aucun' => NULL,
                    'Dégâts de coup critique' => ExoticArmor::CRITICAL_HIT_DAMAGE,
                    'Dégâts de headshot' => ExoticArmor::HEADSHOT_DAMAGE,
                    'Régénération de protection' => ExoticArmor::ARMOR_REGENERATION,
                    'Protection contre altérations d\'état' => ExoticArmor::HAZARD_RESISTANCE,
                    'Santé' => ExoticArmor::HEALTH,
                    'Dégâts de compétence' => ExoticArmor::SKILL_DAMAGE,
                    'Récupération de compétence' => ExoticArmor::SKILL_HASTE,
                    'Compétence de réparation' => ExoticArmor::SKILL_REPAIR,
                ]
            ])
            ->add('mods', ChoiceType::class, [
                'label' => 'Mods',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non' => ExoticArmor::NO,
                    'Oui' => ExoticArmor::YES
                ]
            ])
            ->add('warlords', ChoiceType::class, [
                'label' => 'Apparu avec Warlords ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non apparue avec Warlord Of New York' => ExoticArmor::NO,
                    'Apparue avec Warlords Of New York' => ExoticArmor::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExoticArmor::class,
        ]);
    }
}
