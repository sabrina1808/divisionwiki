<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstName', TextType::class, [
            'label' => 'Votre prénom',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Votre prénom',
                'autofocus' => true
            ]
        ])
        ->add('lastName', TextType::class, [
            'label' => 'Votre nom',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Votre nom'
            ]
        ])
        ->add('email', EmailType::class, [
            'label' => 'Votre adresse de messagerie',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'exemple@exemple.com'
            ]
        ])
        ->add('username', TextType::class, [
            'label' => 'Votre pseudo',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Votre pseudo'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
