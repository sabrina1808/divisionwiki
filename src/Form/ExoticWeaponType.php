<?php

namespace App\Form;

use App\Entity\WeaponType;
use App\Entity\ExoticWeapon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExoticWeaponType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weaponTypeId', EntityType::class, [
                'label' => 'Type d\'arme',
                'class' => WeaponType::class,
                'choice_label' => 'typeName',
                'attr' => [
                    'class' => 'form-control',
                    'autofocus' => true
                ]
            ])
            ->add('variantWeapon', TextType::class, [
                'label' => 'Variante de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Variante de l\'arme'
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'arme',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'arme'
                ]
            ])
            ->add('talent1', TextareaType::class, [
                'label' => 'Talent 1',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent 1'
                ]
            ])
            ->add('talent2', TextareaType::class, [
                'label' => 'Talent 2',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Talent 2'
                ]
            ])
            ->add('mainAttribute1', ChoiceType::class, [
                'label' => 'Attribut essentiel 1',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts de fusil de calibre 12' => ExoticWeapon::SHOTGUN_DAMAGE,
                    'Dégâts de fusil' => ExoticWeapon::RIFLE_DAMAGE,
                    'Dégâts de fusil d\'assaut' => ExoticWeapon::ASSAULT_RIFLE_DAMAGE,
                    'Dégâts de fusil mitrailleur' => ExoticWeapon::LIGHT_MACHINE_GUN_DAMAGE,
                    'Dégâts de fusil de précision' => ExoticWeapon::MARKSMAN_RIFLE_DAMAGE,
                    'Dégâts de pistolet' => ExoticWeapon::PISTOL_DAMAGE,
                    'Dégâts de pistolet-mitrailleur' => ExoticWeapon::SUB_MACHINE_GUN_DAMAGE
                ]
            ])
            ->add('mainAttribute2', ChoiceType::class, [
                'label' => 'Attribut essentiel 2',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Aucun' => NULL,
                    'Dégâts aux protections' => ExoticWeapon::DAMAGE_TO_ARMOR,
                    'Dégâts de coup critique' => ExoticWeapon::CRITICAL_HIT_DAMAGE,
                    'Dégâts à la santé' => ExoticWeapon::HEALTH_DAMAGE,
                    'Dégâts sur cible non abritée' => ExoticWeapon::DAMAGE_TO_TARGET_OUT_OF_COVER,
                    'Dégâts de headshot' => ExoticWeapon::HEADSHOT_DAMAGE,
                    'Probabilité de coup critique' => ExoticWeapon::CRITICAL_HIT_CHANCE
                ]
            ])
            ->add('secondAttribute', ChoiceType::class, [
                'label' => 'Attribut secondaire',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Dégâts sur cible non abritée' => ExoticWeapon::DAMAGE_TO_TARGET_OUT_OF_COVER,
                    'Dégâts de coup critique' => ExoticWeapon::CRITICAL_HIT_DAMAGE,
                    'Probabilité de coup critique' => ExoticWeapon::CRITICAL_HIT_CHANCE,
                    'Dégâts de headshot' => ExoticWeapon::HEADSHOT_DAMAGE,
                    'Dégâts à la santé' => ExoticWeapon::HEADSHOT_DAMAGE,
                    'Dégâts aux protections' => ExoticWeapon::DAMAGE_TO_ARMOR,
                    'Taille du chargeur' => ExoticWeapon::MAGAZINE_SIZE
                ]
            ])
            ->add('optimalRange', IntegerType::class, [
                'label' => 'Portée optimale',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Portée optimale'
                ]
            ])
            ->add('rpm', IntegerType::class, [
                'label' => 'Cadence de tir',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Cadence de tir'
                ]
            ])
            ->add('magSize', IntegerType::class, [
                'label' => 'Taille du chargeur',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Taille du chargeur'
                ]
            ])
            ->add('relaodSpeed', NumberType::class, [
                'label' => 'Vitesse de rechargement',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Vitesse de rechargement'
                ]
            ])
            ->add('headshotMultiplicator', IntegerType::class, [
                'label' => 'Multiplacateur de headshot',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Multiplacateur de headshot'
                ]
            ])
            ->add('baseDamage', IntegerType::class, [
                'label' => 'Dégâts de base',
                'attr' => [
                    'class' => 'form-control',
                    'placehoder' => 'Dégâts de base'
                ]
            ])
            ->add('mods', TextareaType::class, [
                'label' => 'Mods',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Mods'
                ]
            ])
            ->add('warlords', ChoiceType::class, [
                'label' => 'Apparu avec Warlords ?',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => [
                    'Non apparue avec Warlord Of New York' => ExoticWeapon::NO,
                    'Apparue avec Warlords Of New York' => ExoticWeapon::YES
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExoticWeapon::class,
        ]);
    }
}
