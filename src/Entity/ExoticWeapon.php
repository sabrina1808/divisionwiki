<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ExoticWeaponRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

/**
 * @ORM\Entity(repositoryClass=ExoticWeaponRepository::class)
 */
class ExoticWeapon
{
    public const NO = 0;
    public const YES = 1;
    public const SHOTGUN_DAMAGE = "Dégâts de fusil de calibre 12";
    public const RIFLE_DAMAGE = "Dégâts de fusil";
    public const ASSAULT_RIFLE_DAMAGE = "Dégâts de fusil d'assaut";
    public const LIGHT_MACHINE_GUN_DAMAGE = "Dégâts de fusil mitrailleur";
    public const MARKSMAN_RIFLE_DAMAGE = "Dégâts de fusil de précision";
    public const PISTOL_DAMAGE = "Dégâts de pistolet";
    public const SUB_MACHINE_GUN_DAMAGE = "Dégâts de pistolet-mitrailleur";
    public const DAMAGE_TO_ARMOR = "Dégâts aux protections";
    public const CRITICAL_HIT_DAMAGE = "Dégâts de coup critique";
    public const HEALTH_DAMAGE = "Dégâts à la santé";
    public const DAMAGE_TO_TARGET_OUT_OF_COVER = "Dégâts sur cible non abritée";
    public const HEADSHOT_DAMAGE = "Dégâts de headshot";
    public const CRITICAL_HIT_CHANCE = "Probabilité de coup critique";
    public const MAGAZINE_SIZE = "Taille du chargeur";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=WeaponType::class, inversedBy="exoticWeapons")
     */
    private $weaponTypeId;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ variante de l'arme ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ variante de l'arme ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ variante de l'arme ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $variantWeapon;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de l'arme ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ nom de l'arme ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ nom de l'arme ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ talent 1 ne peut pas être vide"
     * )
     * @Length(
     *  max = 600,
     *  maxMessage = "Le champ talent 1 ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôûÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le champ talent 1 ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $talent1;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 600,
     *  maxMessage = "Le champ talent 2 ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôûÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le champ talent 2 ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $talent2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainAttribute1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mainAttribute2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondAttribute;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ portée optimale ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ portée optimale doit être positive"
     * )
     */
    private $optimalRange;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ cadence de tir ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ cadence de tir doit être positive"
     * )
     */
    private $rpm;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ taille du chargeur ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ taille du chargeur doit être positive"
     * )
     */
    private $magSize;

    /**
     * @ORM\Column(type="float")
     * @NotBlank(
     *  message = "Le champ vitesse de rechargement ne peut pas être vide"
     * )
     * @PositiveOrZero(
     *  message = "La valeur du champ vitesse de rechargement doit être positive ou zéro"
     * )
     */
    private $relaodSpeed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Positive(
     *  message = "La valeur du champ multiplicateur de headshot doit être positive"
     * )
     */
    private $headshotMultiplicator;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de base ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de base doit être positive"
     * )
     */
    private $baseDamage;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ mods ne peut pas être vide"
     * )
     * @Length(
     *  max = 500,
     *  maxMessage = "La mods ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôûÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "La mods ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $mods;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $warlords;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeaponTypeId(): ?WeaponType
    {
        return $this->weaponTypeId;
    }

    public function setWeaponTypeId(?WeaponType $weaponTypeId): self
    {
        $this->weaponTypeId = $weaponTypeId;

        return $this;
    }

    public function getVariantWeapon(): ?string
    {
        return $this->variantWeapon;
    }

    public function setVariantWeapon(string $variantWeapon): self
    {
        $this->variantWeapon = $variantWeapon;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTalent1(): ?string
    {
        return $this->talent1;
    }

    public function setTalent1(string $talent1): self
    {
        $this->talent1 = $talent1;

        return $this;
    }

    public function getTalent2(): ?string
    {
        return $this->talent2;
    }

    public function setTalent2(?string $talent2): self
    {
        $this->talent2 = $talent2;

        return $this;
    }

    public function getMainAttribute1(): ?string
    {
        return $this->mainAttribute1;
    }

    public function setMainAttribute1(string $mainAttribute1): self
    {
        $this->mainAttribute1 = $mainAttribute1;

        return $this;
    }

    public function getMainAttribute2(): ?string
    {
        return $this->mainAttribute2;
    }

    public function setMainAttribute2(string $mainAttribute2): self
    {
        $this->mainAttribute2 = $mainAttribute2;

        return $this;
    }

    public function getSecondAttribute(): ?string
    {
        return $this->secondAttribute;
    }

    public function setSecondAttribute(string $secondAttribute): self
    {
        $this->secondAttribute = $secondAttribute;

        return $this;
    }

    public function getOptimalRange(): ?int
    {
        return $this->optimalRange;
    }

    public function setOptimalRange(int $optimalRange): self
    {
        $this->optimalRange = $optimalRange;

        return $this;
    }

    public function getRpm(): ?int
    {
        return $this->rpm;
    }

    public function setRpm(int $rpm): self
    {
        $this->rpm = $rpm;

        return $this;
    }

    public function getMagSize(): ?int
    {
        return $this->magSize;
    }

    public function setMagSize(int $magSize): self
    {
        $this->magSize = $magSize;

        return $this;
    }

    public function getRelaodSpeed(): ?float
    {
        return $this->relaodSpeed;
    }

    public function setRelaodSpeed(float $relaodSpeed): self
    {
        $this->relaodSpeed = $relaodSpeed;

        return $this;
    }

    public function getHeadshotMultiplicator(): ?int
    {
        return $this->headshotMultiplicator;
    }

    public function setHeadshotMultiplicator(int $headshotMultiplicator): self
    {
        $this->headshotMultiplicator = $headshotMultiplicator;

        return $this;
    }

    public function getBaseDamage(): ?int
    {
        return $this->baseDamage;
    }

    public function setBaseDamage(int $baseDamage): self
    {
        $this->baseDamage = $baseDamage;

        return $this;
    }

    public function getMods(): ?string
    {
        return $this->mods;
    }

    public function setMods(string $mods): self
    {
        $this->mods = $mods;

        return $this;
    }

    public function getWarlords(): ?string
    {
        return $this->warlords;
    }

    public function setWarlords(string $warlords): self
    {
        $this->warlords = $warlords;

        return $this;
    }
}
