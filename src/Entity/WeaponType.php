<?php

namespace App\Entity;

use App\Repository\WeaponTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeaponTypeRepository::class)
 */
class WeaponType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeName;

    /**
     * @ORM\OneToMany(targetEntity=WeaponModel::class, mappedBy="weaponTypeId")
     */
    private $weaponModels;

    /**
     * @ORM\OneToMany(targetEntity=NamedWeapon::class, mappedBy="weaponTypeId")
     */
    private $namedWeapons;

    /**
     * @ORM\OneToMany(targetEntity=ExoticWeapon::class, mappedBy="weaponTypeId")
     */
    private $exoticWeapons;

    public function __construct()
    {
        $this->weaponModels = new ArrayCollection();
        $this->namedWeapons = new ArrayCollection();
        $this->exoticWeapons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeName(): ?string
    {
        return $this->typeName;
    }

    public function setTypeName(string $typeName): self
    {
        $this->typeName = $typeName;

        return $this;
    }

    /**
     * @return Collection|WeaponModel[]
     */
    public function getWeaponModels(): Collection
    {
        return $this->weaponModels;
    }

    public function addWeaponModel(WeaponModel $weaponModel): self
    {
        if (!$this->weaponModels->contains($weaponModel)) {
            $this->weaponModels[] = $weaponModel;
            $weaponModel->setWeaponTypeId($this);
        }

        return $this;
    }

    public function removeWeaponModel(WeaponModel $weaponModel): self
    {
        if ($this->weaponModels->removeElement($weaponModel)) {
            // set the owning side to null (unless already changed)
            if ($weaponModel->getWeaponTypeId() === $this) {
                $weaponModel->setWeaponTypeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NamedWeapon[]
     */
    public function getNamedWeapons(): Collection
    {
        return $this->namedWeapons;
    }

    public function addNamedWeapon(NamedWeapon $namedWeapon): self
    {
        if (!$this->namedWeapons->contains($namedWeapon)) {
            $this->namedWeapons[] = $namedWeapon;
            $namedWeapon->setWeaponTypeId($this);
        }

        return $this;
    }

    public function removeNamedWeapon(NamedWeapon $namedWeapon): self
    {
        if ($this->namedWeapons->removeElement($namedWeapon)) {
            // set the owning side to null (unless already changed)
            if ($namedWeapon->getWeaponTypeId() === $this) {
                $namedWeapon->setWeaponTypeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExoticWeapon[]
     */
    public function getExoticWeapons(): Collection
    {
        return $this->exoticWeapons;
    }

    public function addExoticWeapon(ExoticWeapon $exoticWeapon): self
    {
        if (!$this->exoticWeapons->contains($exoticWeapon)) {
            $this->exoticWeapons[] = $exoticWeapon;
            $exoticWeapon->setWeaponTypeId($this);
        }

        return $this;
    }

    public function removeExoticWeapon(ExoticWeapon $exoticWeapon): self
    {
        if ($this->exoticWeapons->removeElement($exoticWeapon)) {
            // set the owning side to null (unless already changed)
            if ($exoticWeapon->getWeaponTypeId() === $this) {
                $exoticWeapon->setWeaponTypeId(null);
            }
        }

        return $this;
    }
}
