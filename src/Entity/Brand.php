<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BrandRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 */
class Brand
{
    public const NO = 0;
    public const YES = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de la marque ne peut pas être vide"
     * )
     * @Length(
     *  max = 50,
     *  maxMessage = "Le nom de la marque ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s&.,-]+$/",
     *  message = "Le nom de la marque ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ bonus 1 pièce ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le Bonus 1 pièce ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-]+$/",
     *  message = "Le Bonus 1 pièce ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus1;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ bonus 2 pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le Bonus 2 pièces ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-]+$/",
     *  message = "Le Bonus 2 pièces ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus2;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ bonus 3 pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le Bonus 3 pièces ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-]+$/",
     *  message = "Le Bonus 3 pièces ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus3;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ description ne peut pas être vide"
     * )
     * @Length(
     *  max = 1000,
     *  maxMessage = "La Description ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%,.\'\-\/:;()]+$/",
     *  message = "La Description ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $warlord;

    /**
     * @ORM\OneToMany(targetEntity=ArmorModel::class, mappedBy="brandId")
     */
    private $armorModels;

    /**
     * @ORM\OneToMany(targetEntity=NamedArmor::class, mappedBy="brandId")
     */
    private $namedArmors;

    public function __construct()
    {
        $this->armorModels = new ArrayCollection();
        $this->namedArmors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBonus1(): ?string
    {
        return $this->bonus1;
    }

    public function setBonus1(string $bonus1): self
    {
        $this->bonus1 = $bonus1;

        return $this;
    }

    public function getBonus2(): ?string
    {
        return $this->bonus2;
    }

    public function setBonus2(string $bonus2): self
    {
        $this->bonus2 = $bonus2;

        return $this;
    }

    public function getBonus3(): ?string
    {
        return $this->bonus3;
    }

    public function setBonus3(string $bonus3): self
    {
        $this->bonus3 = $bonus3;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWarlord(): ?string
    {
        return $this->warlord;
    }

    public function setWarlord(string $warlord): self
    {
        $this->warlord = $warlord;

        return $this;
    }

    /**
     * @return Collection|ArmorModel[]
     */
    public function getArmorModels(): Collection
    {
        return $this->armorModels;
    }

    public function addArmorModel(ArmorModel $armorModel): self
    {
        if (!$this->armorModels->contains($armorModel)) {
            $this->armorModels[] = $armorModel;
            $armorModel->setBrandId($this);
        }

        return $this;
    }

    public function removeArmorModel(ArmorModel $armorModel): self
    {
        if ($this->armorModels->removeElement($armorModel)) {
            // set the owning side to null (unless already changed)
            if ($armorModel->getBrandId() === $this) {
                $armorModel->setBrandId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NamedArmor[]
     */
    public function getNamedArmors(): Collection
    {
        return $this->namedArmors;
    }

    public function addNamedArmor(NamedArmor $namedArmor): self
    {
        if (!$this->namedArmors->contains($namedArmor)) {
            $this->namedArmors[] = $namedArmor;
            $namedArmor->setBrandId($this);
        }

        return $this;
    }

    public function removeNamedArmor(NamedArmor $namedArmor): self
    {
        if ($this->namedArmors->removeElement($namedArmor)) {
            // set the owning side to null (unless already changed)
            if ($namedArmor->getBrandId() === $this) {
                $namedArmor->setBrandId(null);
            }
        }

        return $this;
    }
}
