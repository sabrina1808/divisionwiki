<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\NamedWeaponRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

/**
 * @ORM\Entity(repositoryClass=NamedWeaponRepository::class)
 */
class NamedWeapon
{
    public const DAMAGE_TO_ARMOR = "Dégâts aux protections";
    public const CRITICAL_HIT_DAMAGE = "Dégâts de coup critique";
    public const HEALTH_DAMAGE = "Dégâts à la santé";
    public const DAMAGE_TO_TARGET_OUT_OF_COVER = "Dégâts sur cible non abritée";
    public const HEADSHOT_DAMAGE = "Dégâts de headshot";
    public const CRITICAL_HIT_CHANCE = "Probabilité de coup critique";
    public const RANDOM = "Alétoire";
    public const TIER = "Tier de compétence";
    public const RELOAD_SPEED = "Vitesse de rechargement";
    public const ARMOR_ON_KILL = "Protection pour élimination";
    public const STABILITY = "Stabilité";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=WeaponType::class, inversedBy="namedWeapons")
     */
    private $weaponTypeId;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ variante de l'arme ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ variante de l'arme ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ variante de l'arme ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $variantWeapon;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de l'arme ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ nom de l'arme ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ nom de l'arme ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainAttribute1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mainAttribute2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondAttribute;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ portée optimale ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ portée optimale doit être positive"
     * )
     */
    private $optimalRange;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ cadence de tir ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ cadence de tir doit être positive"
     * )
     */
    private $rpm;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ taille du chargeur ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ taille du chargeur doit être positive"
     * )
     */
    private $magSize;

    /**
     * @ORM\Column(type="float")
     * @NotBlank(
     *  message = "Le champ vitesse de rechargement ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ vitesse de rechargement doit être positive"
     * )
     */
    private $reloadSpeed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Positive(
     *  message = "La valeur du champ multiplicateur de headshot doit être positive"
     * )
     * 
     */
    private $headshotMultiplicator;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de base ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de base doit être positive"
     * )
     */
    private $baseDamage;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 500,
     *  maxMessage = "Le Talent parfait ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäûçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le Talent parfait ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $perfectTalent;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 500,
     *  maxMessage = "La Zone d'obtention ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêëàâäçûïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "La Zone d'obtention ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $dropLocation;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 500,
     *  maxMessage = "Les commentaires ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêëàâäçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Les commentaires ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $comments;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeaponTypeId(): ?WeaponType
    {
        return $this->weaponTypeId;
    }

    public function setWeaponTypeId(?WeaponType $weaponTypeId): self
    {
        $this->weaponTypeId = $weaponTypeId;

        return $this;
    }

    public function getVariantWeapon(): ?string
    {
        return $this->variantWeapon;
    }

    public function setVariantWeapon(string $variantWeapon): self
    {
        $this->variantWeapon = $variantWeapon;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainAttribute1(): ?string
    {
        return $this->mainAttribute1;
    }

    public function setMainAttribute1(string $mainAttribute1): self
    {
        $this->mainAttribute1 = $mainAttribute1;

        return $this;
    }

    public function getMainAttribute2(): ?string
    {
        return $this->mainAttribute2;
    }

    public function setMainAttribute2(?string $mainAttribute2): self
    {
        $this->mainAttribute2 = $mainAttribute2;

        return $this;
    }

    public function getSecondAttribute(): ?string
    {
        return $this->secondAttribute;
    }

    public function setSecondAttribute(string $secondAttribute): self
    {
        $this->secondAttribute = $secondAttribute;

        return $this;
    }

    public function getOptimalRange(): ?int
    {
        return $this->optimalRange;
    }

    public function setOptimalRange(int $optimalRange): self
    {
        $this->optimalRange = $optimalRange;

        return $this;
    }

    public function getRpm(): ?int
    {
        return $this->rpm;
    }

    public function setRpm(int $rpm): self
    {
        $this->rpm = $rpm;

        return $this;
    }

    public function getMagSize(): ?int
    {
        return $this->magSize;
    }

    public function setMagSize(int $magSize): self
    {
        $this->magSize = $magSize;

        return $this;
    }

    public function getReloadSpeed(): ?float
    {
        return $this->reloadSpeed;
    }

    public function setReloadSpeed(float $reloadSpeed): self
    {
        $this->reloadSpeed = $reloadSpeed;

        return $this;
    }

    public function getHeadshotMultiplicator(): ?int
    {
        return $this->headshotMultiplicator;
    }

    public function setHeadshotMultiplicator(?int $headshotMultiplicator): self
    {
        $this->headshotMultiplicator = $headshotMultiplicator;

        return $this;
    }

    public function getBaseDamage(): ?int
    {
        return $this->baseDamage;
    }

    public function setBaseDamage(int $baseDamage): self
    {
        $this->baseDamage = $baseDamage;

        return $this;
    }

    public function getPerfectTalent(): ?string
    {
        return $this->perfectTalent;
    }

    public function setPerfectTalent(?string $perfectTalent): self
    {
        $this->perfectTalent = $perfectTalent;

        return $this;
    }

    public function getDropLocation(): ?string
    {
        return $this->dropLocation;
    }

    public function setDropLocation(?string $dropLocation): self
    {
        $this->dropLocation = $dropLocation;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }
}
