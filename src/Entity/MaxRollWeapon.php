<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MaxRollWeaponRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

/**
 * @ORM\Entity(repositoryClass=MaxRollWeaponRepository::class)
 */
class MaxRollWeapon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=WeaponType::class, cascade={"persist", "remove"})
     */
    private $weaponTypeId;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts d'armes ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts d'armes doit être positive"
     * )
     */
    private $weaponDamage;

    /**
     * @ORM\Column(type="float")
     * @NotBlank(
     *  message = "Le champ dégâts à la santé ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts à la santé doit être positive"
     * )
     */
    private $healthDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts aux protections ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts aux protections doit être positive"
     * )
     */
    private $damageToArmor;

    /**
     * @ORM\Column(type="float")
     * @NotBlank(
     *  message = "Le champ probabilité de coup critique ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ probabilité de coup critique doit être positive"
     * )
     */
    private $criticalHitChance;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts aux cibles non abritées ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts aux cibles non abritées doit être positive"
     * )
     */
    private $damageToTargetOutOfCover;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de headshot ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de headshot doit être positive"
     * )
     */
    private $headshotDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de coup critique ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de coup critique doit être positive"
     * )
     */
    private $criticalHitDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ vitesse de rechargement ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ vitesse de rechargement doit être positive"
     * )
     */
    private $reloadSpeed;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ stabilité ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champtabilité doit être positive"
     * )
     */
    private $stability;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ précision ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ précision doit être positive"
     * )
     */
    private $accurancy;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ portée optimale ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ portée optimale doit être positive"
     * )
     */
    private $optimalRange;

    /**
     * @ORM\Column(type="float")
     * @NotBlank(
     *  message = "Le champ taille du chargeur ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ taille du chargeur doit être positive"
     * )
     */
    private $magazineSize;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ cadence de tir ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ cadence de tir doit être positive"
     * )
     */
    private $rateOfFire;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ vitesse de changement d'arme ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ vitesse de changement d'arme doit être positive"
     * )
     */
    private $swapSpeed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeaponTypeId(): ?WeaponType
    {
        return $this->weaponTypeId;
    }

    public function setWeaponTypeId(?WeaponType $weaponTypeId): self
    {
        $this->weaponTypeId = $weaponTypeId;

        return $this;
    }

    public function getWeaponDamage(): ?int
    {
        return $this->weaponDamage;
    }

    public function setWeaponDamage(int $weaponDamage): self
    {
        $this->weaponDamage = $weaponDamage;

        return $this;
    }

    public function getHealthDamage(): ?float
    {
        return $this->healthDamage;
    }

    public function setHealthDamage(float $healthDamage): self
    {
        $this->healthDamage = $healthDamage;

        return $this;
    }

    public function getDamageToArmor(): ?int
    {
        return $this->damageToArmor;
    }

    public function setDamageToArmor(int $damageToArmor): self
    {
        $this->damageToArmor = $damageToArmor;

        return $this;
    }

    public function getCriticalHitChance(): ?float
    {
        return $this->criticalHitChance;
    }

    public function setCriticalHitChance(float $criticalHitChance): self
    {
        $this->criticalHitChance = $criticalHitChance;

        return $this;
    }

    public function getDamageToTargetOutOfCover(): ?int
    {
        return $this->damageToTargetOutOfCover;
    }

    public function setDamageToTargetOutOfCover(int $damageToTargetOutOfCover): self
    {
        $this->damageToTargetOutOfCover = $damageToTargetOutOfCover;

        return $this;
    }

    public function getHeadshotDamage(): ?int
    {
        return $this->headshotDamage;
    }

    public function setHeadshotDamage(int $headshotDamage): self
    {
        $this->headshotDamage = $headshotDamage;

        return $this;
    }

    public function getCriticalHitDamage(): ?int
    {
        return $this->criticalHitDamage;
    }

    public function setCriticalHitDamage(int $criticalHitDamage): self
    {
        $this->criticalHitDamage = $criticalHitDamage;

        return $this;
    }

    public function getReloadSpeed(): ?int
    {
        return $this->reloadSpeed;
    }

    public function setReloadSpeed(int $reloadSpeed): self
    {
        $this->reloadSpeed = $reloadSpeed;

        return $this;
    }

    public function getStability(): ?int
    {
        return $this->stability;
    }

    public function setStability(int $stability): self
    {
        $this->stability = $stability;

        return $this;
    }

    public function getAccurancy(): ?int
    {
        return $this->accurancy;
    }

    public function setAccurancy(int $accurancy): self
    {
        $this->accurancy = $accurancy;

        return $this;
    }

    public function getOptimalRange(): ?int
    {
        return $this->optimalRange;
    }

    public function setOptimalRange(int $optimalRange): self
    {
        $this->optimalRange = $optimalRange;

        return $this;
    }

    public function getMagazineSize(): ?float
    {
        return $this->magazineSize;
    }

    public function setMagazineSize(float $magazineSize): self
    {
        $this->magazineSize = $magazineSize;

        return $this;
    }

    public function getRateOfFire(): ?int
    {
        return $this->rateOfFire;
    }

    public function setRateOfFire(int $rateOfFire): self
    {
        $this->rateOfFire = $rateOfFire;

        return $this;
    }

    public function getSwapSpeed(): ?int
    {
        return $this->swapSpeed;
    }

    public function setSwapSpeed(int $swapSpeed): self
    {
        $this->swapSpeed = $swapSpeed;

        return $this;
    }
}
