<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ArmorTalentRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=ArmorTalentRepository::class)
 */
class ArmorTalent
{
    public const NO = 0;
    public const YES = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "La champ nom du talent ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le nom du talent ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\s\-']+$/",
     *  message = "Le nom du talent ne peut contenir que des lettres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom du talent (anglais) ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le nom du talent (anglais) ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\s]+$/",
     *  message = "Le nom du talent (anglais) ne peut contenir que des lettres"
     * )
     */
    private $nameEng;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ description ne peut pas être vide"
     * )
     * @Length(
     *  max = 500,
     *  maxMessage = "La Description ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "La Description ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Length(
     *  max = 250,
     *  maxMessage = "Les Pré-requis ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%,.\'\-\/:;()]+$/",
     *  message = "Les Pré-requis ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $requirements;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chest;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $backpack;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameEng(): ?string
    {
        return $this->nameEng;
    }

    public function setNameEng(string $nameEng): self
    {
        $this->nameEng = $nameEng;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRequirements(): ?string
    {
        return $this->requirements;
    }

    public function setRequirements(string $requirements): self
    {
        $this->requirements = $requirements;

        return $this;
    }

    public function getChest(): ?string
    {
        return $this->chest;
    }

    public function setChest(string $chest): self
    {
        $this->chest = $chest;

        return $this;
    }

    public function getBackpack(): ?string
    {
        return $this->backpack;
    }

    public function setBackpack(string $backpack): self
    {
        $this->backpack = $backpack;

        return $this;
    }
}
