<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\NamedArmorRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=NamedArmorRepository::class)
 */
class NamedArmor
{
    public const NO = 0;
    public const YES = 1;
    public const OFFENSIF = "Dégâts d'armes";
    public const DEFENSIF = "Protection";
    public const TIER = "Tier de compétence";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="namedArmors")
     */
    private $brandId;

    /**
     * @ORM\ManyToOne(targetEntity=GearType::class, inversedBy="namedArmors")
     */
    private $gearTypeId;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de l'armure ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ nom de l'armure ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ nom de l'armure ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainAttribute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Length(
     *  max = 100,
     *  maxMessage = "L'attribut unique ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-]+$/",
     *  message = "L'attribut unique ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $uniqueAttribute;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondAttribute1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondAttribute2;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 500,
     *  maxMessage = "Le Talent parfait ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le Talent parfait ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $perfectTalent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mods;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ zone d'obtention ne peut pas être vide"
     * )
     * @Length(
     *  max = 500,
     *  maxMessage = "La Zone d'obtention ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "La Zone d'obtention ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $dropLocation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrandId(): ?Brand
    {
        return $this->brandId;
    }

    public function setBrandId(?Brand $brandId): self
    {
        $this->brandId = $brandId;

        return $this;
    }

    public function getGearTypeId(): ?GearType
    {
        return $this->gearTypeId;
    }

    public function setGearTypeId(?GearType $gearTypeId): self
    {
        $this->gearTypeId = $gearTypeId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainAttribute(): ?string
    {
        return $this->mainAttribute;
    }

    public function setMainAttribute(string $mainAttribute): self
    {
        $this->mainAttribute = $mainAttribute;

        return $this;
    }

    public function getUniqueAttribute(): ?string
    {
        return $this->uniqueAttribute;
    }

    public function setUniqueAttribute(?string $uniqueAttribute): self
    {
        $this->uniqueAttribute = $uniqueAttribute;

        return $this;
    }

    public function getSecondAttribute1(): ?string
    {
        return $this->secondAttribute1;
    }

    public function setSecondAttribute1(string $secondAttribute1): self
    {
        $this->secondAttribute1 = $secondAttribute1;

        return $this;
    }

    public function getSecondAttribute2(): ?string
    {
        return $this->secondAttribute2;
    }

    public function setSecondAttribute2(?string $secondAttribute2): self
    {
        $this->secondAttribute2 = $secondAttribute2;

        return $this;
    }

    public function getPerfectTalent(): ?string
    {
        return $this->perfectTalent;
    }

    public function setPerfectTalent(?string $perfectTalent): self
    {
        $this->perfectTalent = $perfectTalent;

        return $this;
    }

    public function getMods(): ?string
    {
        return $this->mods;
    }

    public function setMods(string $mods): self
    {
        $this->mods = $mods;

        return $this;
    }

    public function getDropLocation(): ?string
    {
        return $this->dropLocation;
    }

    public function setDropLocation(string $dropLocation): self
    {
        $this->dropLocation = $dropLocation;

        return $this;
    }
}
