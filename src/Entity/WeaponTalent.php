<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\WeaponTalentRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=WeaponTalentRepository::class)
 */
class WeaponTalent
{
    public const NO = 0;
    public const YES = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "La champ nom du talent ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le nom du talent ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\s\-'&]+$/",
     *  message = "Le nom du talent ne peut contenir que des lettres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom du talent (anglais) ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le nom du talent (anglais) ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\s\-'&]+$/",
     *  message = "Le nom du talent (anglais) ne peut contenir que des lettres"
     * )
     */
    private $nameEng;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ description ne peut pas être vide"
     * )
     * @Length(
     *  max = 500,
     *  maxMessage = "La Description ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôûÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "La Description ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Length(
     *  max = 250,
     *  maxMessage = "Les Pré-requis ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%,.\'\-\/:;()]+$/",
     *  message = "Les Pré-requis ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $requirements;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rifle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shotgun;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $assaultRifle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lightMachineGun;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marksmanRifle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pistol;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subMachineGun;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameEng(): ?string
    {
        return $this->nameEng;
    }

    public function setNameEng(string $nameEng): self
    {
        $this->nameEng = $nameEng;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRequirements(): ?string
    {
        return $this->requirements;
    }

    public function setRequirements(?string $requirements): self
    {
        $this->requirements = $requirements;

        return $this;
    }

    public function getRifle(): ?string
    {
        return $this->rifle;
    }

    public function setRifle(string $rifle): self
    {
        $this->rifle = $rifle;

        return $this;
    }

    public function getShotgun(): ?string
    {
        return $this->shotgun;
    }

    public function setShotgun(string $shotgun): self
    {
        $this->shotgun = $shotgun;

        return $this;
    }

    public function getAssaultRifle(): ?string
    {
        return $this->assaultRifle;
    }

    public function setAssaultRifle(string $assaultRifle): self
    {
        $this->assaultRifle = $assaultRifle;

        return $this;
    }

    public function getLightMachineGun(): ?string
    {
        return $this->lightMachineGun;
    }

    public function setLightMachineGun(string $lightMachineGun): self
    {
        $this->lightMachineGun = $lightMachineGun;

        return $this;
    }

    public function getMarksmanRifle(): ?string
    {
        return $this->marksmanRifle;
    }

    public function setMarksmanRifle(string $marksmanRifle): self
    {
        $this->marksmanRifle = $marksmanRifle;

        return $this;
    }

    public function getPistol(): ?string
    {
        return $this->pistol;
    }

    public function setPistol(string $pistol): self
    {
        $this->pistol = $pistol;

        return $this;
    }

    public function getSubMachineGun(): ?string
    {
        return $this->subMachineGun;
    }

    public function setSubMachineGun(string $subMachineGun): self
    {
        $this->subMachineGun = $subMachineGun;

        return $this;
    }
}
