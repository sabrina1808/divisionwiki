<?php

namespace App\Entity;

use App\Repository\GearTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GearTypeRepository::class)
 */
class GearType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeName;

    /**
     * @ORM\OneToMany(targetEntity=ArmorModel::class, mappedBy="gearTypeId")
     */
    private $armorModels;

    /**
     * @ORM\OneToMany(targetEntity=NamedArmor::class, mappedBy="gearTypeId")
     */
    private $namedArmors;

    /**
     * @ORM\OneToMany(targetEntity=ExoticArmor::class, mappedBy="gearTypeId")
     */
    private $exoticArmors;

    /**
     * @ORM\OneToMany(targetEntity=GearSetModel::class, mappedBy="gearTypeId")
     */
    private $gearSetModels;

    public function __construct()
    {
        $this->armorModels = new ArrayCollection();
        $this->namedArmors = new ArrayCollection();
        $this->exoticArmors = new ArrayCollection();
        $this->gearSetModels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeName(): ?string
    {
        return $this->typeName;
    }

    public function setTypeName(string $typeName): self
    {
        $this->typeName = $typeName;

        return $this;
    }

    /**
     * @return Collection|ArmorModel[]
     */
    public function getArmorModels(): Collection
    {
        return $this->armorModels;
    }

    public function addArmorModel(ArmorModel $armorModel): self
    {
        if (!$this->armorModels->contains($armorModel)) {
            $this->armorModels[] = $armorModel;
            $armorModel->setGearTypeId($this);
        }

        return $this;
    }

    public function removeArmorModel(ArmorModel $armorModel): self
    {
        if ($this->armorModels->removeElement($armorModel)) {
            // set the owning side to null (unless already changed)
            if ($armorModel->getGearTypeId() === $this) {
                $armorModel->setGearTypeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NamedArmor[]
     */
    public function getNamedArmors(): Collection
    {
        return $this->namedArmors;
    }

    public function addNamedArmor(NamedArmor $namedArmor): self
    {
        if (!$this->namedArmors->contains($namedArmor)) {
            $this->namedArmors[] = $namedArmor;
            $namedArmor->setGearTypeId($this);
        }

        return $this;
    }

    public function removeNamedArmor(NamedArmor $namedArmor): self
    {
        if ($this->namedArmors->removeElement($namedArmor)) {
            // set the owning side to null (unless already changed)
            if ($namedArmor->getGearTypeId() === $this) {
                $namedArmor->setGearTypeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExoticArmor[]
     */
    public function getExoticArmors(): Collection
    {
        return $this->exoticArmors;
    }

    public function addExoticArmor(ExoticArmor $exoticArmor): self
    {
        if (!$this->exoticArmors->contains($exoticArmor)) {
            $this->exoticArmors[] = $exoticArmor;
            $exoticArmor->setGearTypeId($this);
        }

        return $this;
    }

    public function removeExoticArmor(ExoticArmor $exoticArmor): self
    {
        if ($this->exoticArmors->removeElement($exoticArmor)) {
            // set the owning side to null (unless already changed)
            if ($exoticArmor->getGearTypeId() === $this) {
                $exoticArmor->setGearTypeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GearSetModel[]
     */
    public function getGearSetModels(): Collection
    {
        return $this->gearSetModels;
    }

    public function addGearSetModel(GearSetModel $gearSetModel): self
    {
        if (!$this->gearSetModels->contains($gearSetModel)) {
            $this->gearSetModels[] = $gearSetModel;
            $gearSetModel->setGearTypeId($this);
        }

        return $this;
    }

    public function removeGearSetModel(GearSetModel $gearSetModel): self
    {
        if ($this->gearSetModels->removeElement($gearSetModel)) {
            // set the owning side to null (unless already changed)
            if ($gearSetModel->getGearTypeId() === $this) {
                $gearSetModel->setGearTypeId(null);
            }
        }

        return $this;
    }
}
