<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @NotBlank(
     *  message = "Le champ pseudo ne peut pas être vide"
     * )
     * @Length(
     *  max = 50,
     *  maxMessage = "Le pseudo ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\-\s]+$/",
     *  message = "Le pseudo ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @NotBlank(
     *  message = "Le champ mot de passe ne peut pas être vide"
     * )
     * @Length(
     *  min = 6,
     *  minMessage = "Le mot de passe doit comprendre au moins {{ limit }} caractère"
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ prénom ne peut pas être vide"
     * )
     * @Length(
     *  max = 50,
     *  maxMessage = "Le prénom ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\-\s]+$/",
     *  message = "Le prénom ne peut contenir que des lettres"
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom ne peut pas être vide"
     * )
     * @Length(
     *  max = 50,
     *  maxMessage = "Le nom ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ\-\s]+$/",
     *  message = "Le nom ne peut contenir que des lettres"
     * )
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ adresse de messagerie ne peut pas être vide"
     * )
     * @Email(
     *  message = "Cette adresse mail n'est pas valide"
     * )
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
