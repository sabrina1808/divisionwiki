<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GearSetModelRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=GearSetModelRepository::class)
 */
class GearSetModel
{
    public const NO = 0;
    public const YES = 1;
    public const OFFENSIF = "Dégâts d'armes";
    public const DEFENSIF = "Protection";
    public const TIER = "Tier de compétence";
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=GearType::class, inversedBy="gearSetModels")
     */
    private $gearTypeId;

    /**
     * @ORM\ManyToOne(targetEntity=GearSet::class, inversedBy="gearSetModels")
     */
    private $gearSetId;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de l'armure ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ nom de l'armure ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ nom de l'armure ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainAttribute;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondAttribute;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mods;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGearTypeId(): ?GearType
    {
        return $this->gearTypeId;
    }

    public function setGearTypeId(?GearType $gearTypeId): self
    {
        $this->gearTypeId = $gearTypeId;

        return $this;
    }

    public function getGearSetId(): ?GearSet
    {
        return $this->gearSetId;
    }

    public function setGearSetId(?GearSet $gearSetId): self
    {
        $this->gearSetId = $gearSetId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainAttribute(): ?string
    {
        return $this->mainAttribute;
    }

    public function setMainAttribute(string $mainAttribute): self
    {
        $this->mainAttribute = $mainAttribute;

        return $this;
    }

    public function getSecondAttribute(): ?string
    {
        return $this->secondAttribute;
    }

    public function setSecondAttribute(string $secondAttribute): self
    {
        $this->secondAttribute = $secondAttribute;

        return $this;
    }

    public function getMods(): ?string
    {
        return $this->mods;
    }

    public function setMods(string $mods): self
    {
        $this->mods = $mods;

        return $this;
    }
}
