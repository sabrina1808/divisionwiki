<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MaxRollArmorRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

/**
 * @ORM\Entity(repositoryClass=MaxRollArmorRepository::class)
 */
class MaxRollArmor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=GearType::class, cascade={"persist", "remove"})
     */
    private $gearTypeId;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts d'armes ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts d'armes doit être positive"
     * )
     */
    private $weaponDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ armure ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ armure doit être positive"
     * )
     */
    private $armor;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ tier de compétence ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ tier de compétence doit être positive"
     * )
     */
    private $skillTier;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ probabilité de coup critique ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ probabilité de coup critique doit être positive"
     * )
     */
    private $criticalHitChance;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de coup critique ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de coup critique doit être positive"
     * )
     */
    private $criticalHitDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de headshot ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de headshot doit être positive"
     * )
     */
    private $headshotDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ maniement d'arme ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ maniement d'arme doit être positive"
     * )
     */
    private $weaponHandling;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ régénération de protection ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ régénération de protection doit être positive"
     * )
     */
    private $armorRegeneration;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ résistances - explosions ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ résistances - explosions doit être positive"
     * )
     */
    private $eplosiveResistance;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ protection contre les altérations d'états ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ protection contre les altérations d'états doit être positive"
     * )
     */
    private $hazardResistance;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ santé ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ santé doit être positive"
     * )
     */
    private $health;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ dégâts de compétence ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ dégâts de compétence doit être positive"
     * )
     */
    private $skillDamage;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ récupération de compétence ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ récupération de compétence doit être positive"
     * )
     */
    private $skillHate;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ compétence de réparation ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ compétence de réparation doit être positive"
     * )
     */
    private $skillRepair;

    /**
     * @ORM\Column(type="integer")
     * @NotBlank(
     *  message = "Le champ effets d'états ne peut pas être vide"
     * )
     * @Positive(
     *  message = "La valeur du champ effets d'états doit être positive"
     * )
     */
    private $statusEffects;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGearTypeId(): ?GearType
    {
        return $this->gearTypeId;
    }

    public function setGearTypeId(?GearType $gearTypeId): self
    {
        $this->gearTypeId = $gearTypeId;

        return $this;
    }

    public function getWeaponDamage(): ?int
    {
        return $this->weaponDamage;
    }

    public function setWeaponDamage(int $weaponDamage): self
    {
        $this->weaponDamage = $weaponDamage;

        return $this;
    }

    public function getArmor(): ?int
    {
        return $this->armor;
    }

    public function setArmor(int $armor): self
    {
        $this->armor = $armor;

        return $this;
    }

    public function getSkillTier(): ?int
    {
        return $this->skillTier;
    }

    public function setSkillTier(int $skillTier): self
    {
        $this->skillTier = $skillTier;

        return $this;
    }

    public function getCriticalHitChance(): ?int
    {
        return $this->criticalHitChance;
    }

    public function setCriticalHitChance(int $criticalHitChance): self
    {
        $this->criticalHitChance = $criticalHitChance;

        return $this;
    }

    public function getCriticalHitDamage(): ?int
    {
        return $this->criticalHitDamage;
    }

    public function setCriticalHitDamage(int $criticalHitDamage): self
    {
        $this->criticalHitDamage = $criticalHitDamage;

        return $this;
    }

    public function getHeadshotDamage(): ?int
    {
        return $this->headshotDamage;
    }

    public function setHeadshotDamage(int $headshotDamage): self
    {
        $this->headshotDamage = $headshotDamage;

        return $this;
    }

    public function getWeaponHandling(): ?int
    {
        return $this->weaponHandling;
    }

    public function setWeaponHandling(int $weaponHandling): self
    {
        $this->weaponHandling = $weaponHandling;

        return $this;
    }

    public function getArmorRegeneration(): ?int
    {
        return $this->armorRegeneration;
    }

    public function setArmorRegeneration(int $armorRegeneration): self
    {
        $this->armorRegeneration = $armorRegeneration;

        return $this;
    }

    public function getEplosiveResistance(): ?int
    {
        return $this->eplosiveResistance;
    }

    public function setEplosiveResistance(int $eplosiveResistance): self
    {
        $this->eplosiveResistance = $eplosiveResistance;

        return $this;
    }

    public function getHazardResistance(): ?int
    {
        return $this->hazardResistance;
    }

    public function setHazardResistance(int $hazardResistance): self
    {
        $this->hazardResistance = $hazardResistance;

        return $this;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getSkillDamage(): ?int
    {
        return $this->skillDamage;
    }

    public function setSkillDamage(int $skillDamage): self
    {
        $this->skillDamage = $skillDamage;

        return $this;
    }

    public function getSkillHate(): ?int
    {
        return $this->skillHate;
    }

    public function setSkillHate(int $skillHate): self
    {
        $this->skillHate = $skillHate;

        return $this;
    }

    public function getSkillRepair(): ?int
    {
        return $this->skillRepair;
    }

    public function setSkillRepair(int $skillRepair): self
    {
        $this->skillRepair = $skillRepair;

        return $this;
    }

    public function getStatusEffects(): ?int
    {
        return $this->statusEffects;
    }

    public function setStatusEffects(int $statusEffects): self
    {
        $this->statusEffects = $statusEffects;

        return $this;
    }
}
