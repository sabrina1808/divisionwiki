<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\GearSetRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=GearSetRepository::class)
 */
class GearSet
{
    public const NO = 0;
    public const YES = 1;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ Nom du gear set est obligatoire"
     * )
     * @Length(
     *  max = 50,
     *  maxMessage = "Le nom du gear set ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s]+$/",
     *  message = "Le nom du gear set ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ bonus 2 pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le Bonus 2 pièces ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-\/]+$/",
     *  message = "Le Bonus 2 pièces ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus1;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ bonus 3 pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le Bonus 3 pièces ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîÉÊËÏÎ0-9\s%'-\/]+$/",
     *  message = "Le Bonus 3 pièces ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus2;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ bonus 4 pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 1000,
     *  maxMessage = "Le Bonus 4 pièces ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le Bonus 4 pièces ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $bonus3;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ talent de torse pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 250,
     *  maxMessage = "Le Talent de torse ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÉÊËÏÎ0-9\s%,.\'\-:;+%]+$/",
     *  message = "Le Talent de torse ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $chestTalent;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ talent de sac à dos pièces ne peut pas être vide"
     * )
     * @Length(
     *  max = 250,
     *  maxMessage = "Le Talent de sac à dos ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäçïîôÉÊËÏÎ0-9\s%,.\'\-:;+%]+$/",
     *  message = "Le Talent de sac à dos ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $backpackTalent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $warlord;

    /**
     * @ORM\OneToMany(targetEntity=GearSetModel::class, mappedBy="gearSetId")
     */
    private $gearSetModels;

    public function __construct()
    {
        $this->gearSetModels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBonus1(): ?string
    {
        return $this->bonus1;
    }

    public function setBonus1(string $bonus1): self
    {
        $this->bonus1 = $bonus1;

        return $this;
    }

    public function getBonus2(): ?string
    {
        return $this->bonus2;
    }

    public function setBonus2(string $bonus2): self
    {
        $this->bonus2 = $bonus2;

        return $this;
    }

    public function getBonus3(): ?string
    {
        return $this->bonus3;
    }

    public function setBonus3(string $bonus3): self
    {
        $this->bonus3 = $bonus3;

        return $this;
    }

    public function getChestTalent(): ?string
    {
        return $this->chestTalent;
    }

    public function setChestTalent(string $chestTalent): self
    {
        $this->chestTalent = $chestTalent;

        return $this;
    }

    public function getBackpackTalent(): ?string
    {
        return $this->backpackTalent;
    }

    public function setBackpackTalent(string $backpackTalent): self
    {
        $this->backpackTalent = $backpackTalent;

        return $this;
    }

    public function getWarlord(): ?string
    {
        return $this->warlord;
    }

    public function setWarlord(string $warlord): self
    {
        $this->warlord = $warlord;

        return $this;
    }

    /**
     * @return Collection|GearSetModel[]
     */
    public function getGearSetModels(): Collection
    {
        return $this->gearSetModels;
    }

    public function addGearSetModel(GearSetModel $gearSetModel): self
    {
        if (!$this->gearSetModels->contains($gearSetModel)) {
            $this->gearSetModels[] = $gearSetModel;
            $gearSetModel->setGearSetId($this);
        }

        return $this;
    }

    public function removeGearSetModel(GearSetModel $gearSetModel): self
    {
        if ($this->gearSetModels->removeElement($gearSetModel)) {
            // set the owning side to null (unless already changed)
            if ($gearSetModel->getGearSetId() === $this) {
                $gearSetModel->setGearSetId(null);
            }
        }

        return $this;
    }
}
