<?php

namespace App\Entity;

use App\Entity\GearType;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ExoticArmorRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass=ExoticArmorRepository::class)
 */
class ExoticArmor
{
    public const NO = 0;
    public const YES = 1;
    public const OFFENSIF = "Dégâts d'armes";
    public const DEFENSIF = "Protection";
    public const TIER = "Tier de compétence";
    public const ALLATTR = "Dégâts d'armes/Protection/Tier de compétence";
    public const CRITICAL_HIT_CHANCE = "Probabilité de coup critique";
    public const CRITICAL_HIT_DAMAGE = "Dégâts de coup critique";
    public const HEADSHOT_DAMAGE = "Dégâts de headshot";
    public const WEAPON_HANDLING = "Maniement d'arme";
    public const ARMOR_REGENERATION = "Régénération de protection";
    public const EXPLOSIVE_RESISTANCE = "Résistances explosions";
    public const HAZARD_RESISTANCE = "Protection contre altérations d'état";
    public const HEALTH = "Santé";
    public const SKILL_DAMAGE = "Dégâts de compétence";
    public const SKILL_REPAIR = "Compétence de réparation";
    public const SKILL_HASTE = "Récupération de compétence";
    public const STATUS_EFFECT = "Effet d'état";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=GearType::class, inversedBy="exoticArmors")
     */
    private $gearTypeId;

    /**
     * @ORM\Column(type="string", length=255)
     * @NotBlank(
     *  message = "Le champ nom de l'armure ne peut pas être vide"
     * )
     * @Length(
     *  max = 100,
     *  maxMessage = "Le champ nom de l'armure ne peut pas dépasser {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäöçïîÉÊËÏÎ0-9\s\'\-\/&.]+$/",
     *  message = "Le champ nom de l'armure ne peut contenir que des lettres et des chiffres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @NotBlank(
     *  message = "Le champ Talent ne peut pas être vide"
     * )
     * @Length(
     *  max = 1000,
     *  maxMessage = "Le Talent ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäûçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+!]+$/",
     *  message = "Le Talent ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $talent1;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Length(
     *  max = 500,
     *  maxMessage = "Le Second talent ne pas contenir plus de {{ limit }} caractères"
     * )
     * @Regex(
     *  pattern = "/^[A-Za-zéèêàâäûçïîôÀÉÊËÏÎ0-9\s%,.\'\-\/:;()+]+$/",
     *  message = "Le Second talent ne peuvent contenir que des lettres et des chiffres"
     * )
     */
    private $talent2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainAttribute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondAttribute1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondAttribute2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mods;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $warlords;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGearTypeId(): ?GearType
    {
        return $this->gearTypeId;
    }

    public function setGearTypeId(?GearType $gearTypeId): self
    {
        $this->gearTypeId = $gearTypeId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTalent1(): ?string
    {
        return $this->talent1;
    }

    public function setTalent1(string $talent1): self
    {
        $this->talent1 = $talent1;

        return $this;
    }

    public function getTalent2(): ?string
    {
        return $this->talent2;
    }

    public function setTalent2(?string $talent2): self
    {
        $this->talent2 = $talent2;

        return $this;
    }

    public function getMainAttribute(): ?string
    {
        return $this->mainAttribute;
    }

    public function setMainAttribute(string $mainAttribute): self
    {
        $this->mainAttribute = $mainAttribute;

        return $this;
    }

    public function getSecondAttribute1(): ?string
    {
        return $this->secondAttribute1;
    }

    public function setSecondAttribute1(?string $secondAttribute1): self
    {
        $this->secondAttribute1 = $secondAttribute1;

        return $this;
    }

    public function getSecondAttribute2(): ?string
    {
        return $this->secondAttribute2;
    }

    public function setSecondAttribute2(?string $secondAttribute2): self
    {
        $this->secondAttribute2 = $secondAttribute2;

        return $this;
    }

    public function getMods(): ?string
    {
        return $this->mods;
    }

    public function setMods(string $mods): self
    {
        $this->mods = $mods;

        return $this;
    }

    public function getWarlords(): ?string
    {
        return $this->warlords;
    }

    public function setWarlords(string $warlords): self
    {
        $this->warlords = $warlords;

        return $this;
    }
}
