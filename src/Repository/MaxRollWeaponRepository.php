<?php

namespace App\Repository;

use App\Entity\MaxRollWeapon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaxRollWeapon|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaxRollWeapon|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaxRollWeapon[]    findAll()
 * @method MaxRollWeapon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaxRollWeaponRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaxRollWeapon::class);
    }

    // /**
    //  * @return MaxRollWeapon[] Returns an array of MaxRollWeapon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaxRollWeapon
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
