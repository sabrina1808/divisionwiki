<?php

namespace App\Repository;

use App\Entity\WeaponModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeaponModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeaponModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeaponModel[]    findAll()
 * @method WeaponModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeaponModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeaponModel::class);
    }

    /**
     * @param string $name  Nom de la variante de l'arme
     * @param integer $id   Identifiant de l'arme affichée
     * @return WeaponModel[] Returns an array of WeaponModel objects
     */
    public function getAllWeaponByVariantName(string $name, int $id)
    {
        return $this->createQueryBuilder('w')
            ->Where('w.variantWeapon = :name')
            ->andWhere('w.id != :id')
            ->setParameter('name', $name)
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?WeaponModel
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
