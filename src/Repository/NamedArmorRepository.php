<?php

namespace App\Repository;

use App\Entity\NamedArmor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NamedArmor|null find($id, $lockMode = null, $lockVersion = null)
 * @method NamedArmor|null findOneBy(array $criteria, array $orderBy = null)
 * @method NamedArmor[]    findAll()
 * @method NamedArmor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NamedArmorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NamedArmor::class);
    }

    /**
     * @param string $talent    Nom du talent de l'armure
     * @return NamedArmor[] Returns an array of NamedArmor objects
     */
    public function findPerfectTalent($talent)
    {
        return $this->createQueryBuilder('n')
            ->Where('n.perfectTalent LIKE :talent')
            ->setParameter('talent', '%'.$talent.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?NamedArmor
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
