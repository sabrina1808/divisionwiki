<?php

namespace App\Repository;

use App\Entity\NamedWeapon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NamedWeapon|null find($id, $lockMode = null, $lockVersion = null)
 * @method NamedWeapon|null findOneBy(array $criteria, array $orderBy = null)
 * @method NamedWeapon[]    findAll()
 * @method NamedWeapon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NamedWeaponRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NamedWeapon::class);
    }

    /**
     * @param string $talent    Nom du talent de l'arme
     * @return NamedWeapon[] Returns an array of NamedWeapon objects
     */
    public function findPerfectTalent($talent)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.perfectTalent LIKE :talent')
            ->setParameter('talent', '%'.$talent.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?NamedWeapon
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
