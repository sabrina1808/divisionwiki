<?php

namespace App\Repository;

use App\Entity\GearSetModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GearSetModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method GearSetModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method GearSetModel[]    findAll()
 * @method GearSetModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GearSetModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GearSetModel::class);
    }

    /**
     * @param integer $gearSetId    Identifiant du modèle de gearSet
     * @return GearSetModel[] Returns an array of GearSetModel objects
     */
    public function getAllGearSetModelByGearSetId(int $gearSetId)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.gearSetId = :gearSetId')
            ->setParameter('gearSetId', $gearSetId)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?GearSetModel
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
