<?php

namespace App\Repository;

use App\Entity\ArmorModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArmorModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArmorModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArmorModel[]    findAll()
 * @method ArmorModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArmorModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArmorModel::class);
    }

    /**
     * @param integer $brandId  Identifiant de la marque
     * @return ArmorModel[] Returns an array of ArmorModel objects
     */
    public function getAllArmorByBrandId(int $brandId)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.brandId = :brandId')
            ->setParameter('brandId', $brandId)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?ArmorModel
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
