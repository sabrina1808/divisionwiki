<?php

namespace App\Repository;

use App\Entity\ArmorTalent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArmorTalent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArmorTalent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArmorTalent[]    findAll()
 * @method ArmorTalent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArmorTalentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArmorTalent::class);
    }

    /**
    * @return ArmorTalent[] Returns an array of ArmorTalent objects
    */
    public function getAllArmorTalentByAlphabeticOrder()
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.name')
            ->orderBy('a.name')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return ArmorTalent[] Returns an array of ArmorTalent objects
    */
    public function getAllChestTalent()
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.name')
            ->where('a.chest = 1')
            ->orderBy('a.name')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return ArmorTalent[] Returns an array of ArmorTalent objects
    */
    public function getAllBackpackTalent()
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.name')
            ->where('a.backpack = 1')
            ->orderBy('a.name')
            ->getQuery()
            ->getResult()
        ;
    }
}
