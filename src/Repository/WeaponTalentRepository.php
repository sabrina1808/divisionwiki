<?php

namespace App\Repository;

use App\Entity\WeaponTalent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeaponTalent|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeaponTalent|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeaponTalent[]    findAll()
 * @method WeaponTalent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeaponTalentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeaponTalent::class);
    }

    /**
     * @return WeaponTalent[] Returns an array of WeaponTalent objects
     */
    public function getAllWeaponTalentByAlphabeticOrder()
    {
        return $this->createQueryBuilder('w')
            ->select('w.id', 'w.name')
            ->orderBy('w.name')
            ->getQuery()
            ->getResult()
        ;
    }
}
