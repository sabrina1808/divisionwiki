<?php

namespace App\Repository;

use App\Entity\MaxRollArmor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaxRollArmor|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaxRollArmor|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaxRollArmor[]    findAll()
 * @method MaxRollArmor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaxRollArmorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaxRollArmor::class);
    }

    // /**
    //  * @return MaxRollArmor[] Returns an array of MaxRollArmor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaxRollArmor
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
