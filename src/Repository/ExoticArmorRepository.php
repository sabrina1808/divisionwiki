<?php

namespace App\Repository;

use App\Entity\ExoticArmor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExoticArmor|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExoticArmor|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExoticArmor[]    findAll()
 * @method ExoticArmor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExoticArmorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExoticArmor::class);
    }

    // /**
    //  * @return ExoticArmor[] Returns an array of ExoticArmor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExoticArmor
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
