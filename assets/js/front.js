document.addEventListener('DOMContentLoaded', () => {
  console.log('JS font load');

  // Élements du menu FRONT
  let gameInfo = document.getElementById('game-informations');
  let userInfo = document.getElementById('user-informations');

  // Apparition du menu des données de jeu
  document.querySelector('.donnees').addEventListener('click', () => {
    gameInfo.classList.toggle('visible');
  });

  // Apparition du menu des informations personnelles
  if (document.querySelector('.compte')) {
    document.querySelector('.compte').addEventListener('click', () => {
      userInfo.classList.toggle('visible');
    });
  }

  // Ajout des armes et armures en favoris
  let url = window.location.pathname;
  let favoriteArmor = document.querySelector('.add-favorite-armor');
  let favoriteWeapon = document.querySelector('.add-favorite-weapon');

  function addArmorToFavorite() {
    let armorsList = [];
    if (localStorage.hasOwnProperty('division-wiki')) {
      armorsList = JSON.parse(localStorage.getItem('division-wiki'));
    }
    let armorName = document.querySelector('h1').textContent;
    let armorUrl = url;

    armorsList.push({
      name: armorName,
      url: armorUrl,
      type: 'armor',
    });

    localStorage.setItem('division-wiki', JSON.stringify(armorsList));
  }

  if (favoriteArmor) {
    favoriteArmor.addEventListener('click', () => {
      addArmorToFavorite();
      document.querySelector('.add-favorite-armor').style.display = 'none';
      document.querySelector('.favorite').style.display = 'block';
    });
  }

  function addWeaponToFavorite() {
    let weaponList = [];
    if (localStorage.hasOwnProperty('division-wiki')) {
      weaponList = JSON.parse(localStorage.getItem('division-wiki'));
    }
    let weaponName = document.querySelector('h1').textContent;
    let weaponUrl = url;

    weaponList.push({
      name: weaponName,
      url: weaponUrl,
      type: 'weapon',
    });

    localStorage.setItem('division-wiki', JSON.stringify(weaponList));
  }

  if (favoriteWeapon) {
    favoriteWeapon.addEventListener('click', () => {
      addWeaponToFavorite();
      document.querySelector('.add-favorite-weapon').style.display = 'none';
      document.querySelector('.favorite').style.display = 'block';
    });
  }

  // Affichage des favoris dans la page "Mes favoris"
  if (url.includes('favorites')) {
    let armorTable = document.querySelector('#armor-list tbody');
    let weaponTable = document.querySelector('#weapon-list tbody');

    let armors = [];
    let weapons = [];
    let items = JSON.parse(localStorage.getItem('division-wiki'));
    if (items != null) {
      items.forEach((item) => {
        switch (item.type) {
          case 'armor':
            armors.push(item);
            break;
          case 'weapon':
            weapons.push(item);
            break;
        }
      });

      if (armors.length != 0) {
        for (let i = 0; i < armors.length; i++) {
          let tableContent = `
            <td>${armors[i].name}</td>
            <td><a href="${armors[i].url}">Voir l'armure</a></td>
          `;

          let tr = document.createElement('tr');
          tr.innerHTML = tableContent;
          armorTable.appendChild(tr);
        }
      } else {
        let tableContent = `<tr>
          <td colspan="2">Aucun favoris</td>
        </tr>`;
        armorTable.innerHTML = tableContent;
      }

      if (weapons.length != 0) {
        for (let i = 0; i < weapons.length; i++) {
          let tableContent = `
            <td>${weapons[i].name}</td>
            <td><a href="${weapons[i].url}">Voir l'arme</a></td>
          `;

          let tr = document.createElement('tr');
          tr.innerHTML = tableContent;
          weaponTable.appendChild(tr);
        }
      } else {
        let tableContent = `<tr>
          <td colspan="2">Aucun favoris</td>
        </tr>`;
        weaponTable.innerHTML = tableContent;
      }
    } else {
      let tableContent = `<tr>
        <td colspan="2">Aucun favoris</td>
      </tr>`;
      armorTable.innerHTML = tableContent;
      weaponTable.innerHTML = tableContent;
    }
  }

  // Modification de l'affichage du bouton des favoris si déjà dans les favoris
  if (url.includes('armors') || url.includes('weapons')) {
    let items = JSON.parse(localStorage.getItem('division-wiki'));
    let gearName = document.querySelector('h1').textContent;

    if (items != null) {
      items.forEach((item) => {
        if (item.name == gearName && item.type == 'armor') {
          document.querySelector('.add-favorite-armor').style.display = 'none';
          document.querySelector('.favorite').style.display = 'block';
        }
        if (item.name == gearName && item.type == 'weapon') {
          document.querySelector('.add-favorite-weapon').style.display = 'none';
          document.querySelector('.favorite').style.display = 'block';
        }
      });
    }
  }
});
