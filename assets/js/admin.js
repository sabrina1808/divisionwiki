document.addEventListener('DOMContentLoaded', () => {
  console.log('admin JS load');

  // Récupération de l'url sous forme de tableau
  let url = window.location.pathname.split('/');

  // Élements du menu ADMIN
  let brandList = document.getElementById('brands-list');
  let armorList = document.getElementById('armors-list');
  let weaponList = document.getElementById('weapons-list');
  let talentList = document.getElementById('talents-list');

  if (url.includes('brand') || url.includes('gearSet')) {
    brandList.style.display = 'block';
  }

  if (url.includes('armorTalent') || url.includes('weaponTalent')) {
    talentList.style.display = 'block';
  }

  if (
    url.includes('armorModel') ||
    url.includes('namedArmor') ||
    url.includes('exoticArmor') ||
    url.includes('gearSetModel') ||
    url.includes('maxRollArmor')
  ) {
    armorList.style.display = 'block';
  }

  if (
    url.includes('weaponModel') ||
    url.includes('namedWeapon') ||
    url.includes('exoticWeapon') ||
    url.includes('maxRollWeapon')
  ) {
    weaponList.style.display = 'block';
  }
});
